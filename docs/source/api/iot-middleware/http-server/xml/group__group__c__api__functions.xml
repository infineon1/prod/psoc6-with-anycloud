<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__c__api__functions" kind="group">
    <compoundname>group_c_api_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga74c8a85310d1537177f80adaef20cfd9" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_network_init</definition>
        <argsstring>(void)</argsstring>
        <name>cy_http_server_network_init</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>One-time initialization function for network sockets implementation. </para>        </briefdescription>
        <detaileddescription>
<para><bold>It must be called once (and only once) before calling any other function in this library.</bold> <simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="333" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga1922ac08bf92b285a8f96bb6170291af" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_network_deinit</definition>
        <argsstring>(void)</argsstring>
        <name>cy_http_server_network_deinit</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>One-time deinitialization function for Secure Sockets implementation. </para>        </briefdescription>
        <detaileddescription>
<para>It should be called after destroying all network socket connections. <simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="340" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_create</definition>
        <argsstring>(cy_network_interface_t *interface, uint16_t port, uint16_t max_connection, cy_https_server_security_info_t *security_info, cy_http_server_t *server_handle)</argsstring>
        <name>cy_http_server_create</name>
        <param>
          <type>cy_network_interface_t *</type>
          <declname>interface</declname>
        </param>
        <param>
          <type>uint16_t</type>
          <declname>port</declname>
        </param>
        <param>
          <type>uint16_t</type>
          <declname>max_connection</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__https__server__security__info__t">cy_https_server_security_info_t</ref> *</type>
          <declname>security_info</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref> *</type>
          <declname>server_handle</declname>
        </param>
        <briefdescription>
<para>Creates a HTTP server instance and initializes its members based on the supplied arguments. </para>        </briefdescription>
        <detaileddescription>
<para>Handle to the HTTP server instance is returned via the handle pointer supplied by the user on successful return. Handle to the HTTP server instance is used for server start, stop, and delete. This API function must be called before using any other HTTP Server API.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">interface</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the network interface information structure (included as part of cy_nw_helper.h). Used for server start. </para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Refer code snippet &lt;a href="index.html#index_1snip6"&gt;Code Snippet 6: HTTP Server Start&lt;/a&gt; to understand how to initialize the 'interface' before calling this function. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">port</parametername>
</parameternamelist>
<parameterdescription>
<para>: Port number on which the server listens for client connection requests. Usually port number 443 is used for the HTTPS server, and port number 80 is used for the HTTP server. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">max_connection</parametername>
</parameternamelist>
<parameterdescription>
<para>: Maximum number of client connections that can be accepted. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">security_info</parametername>
</parameternamelist>
<parameterdescription>
<para>: Security info containing the certificate, private key, and rootCA certificate. For a non-secured connection, this parameter should be NULL. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to store the HTTP sever handle allocated by this function on successful return.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="357" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga13251efe44c82d506b1ccf48e5acc5dd" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_delete</definition>
        <argsstring>(cy_http_server_t server_handle)</argsstring>
        <name>cy_http_server_delete</name>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref></type>
          <declname>server_handle</declname>
        </param>
        <briefdescription>
<para>Deletes the given HTTP server instance and resources allocated for the instance by the <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref> function. </para>        </briefdescription>
        <detaileddescription>
<para>Before calling this API function, the HTTP server associated with server_handle must be stopped.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP server handle </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="370" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga3cb020a5e45f80b55ea3fc2eb1c6c3df" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_start</definition>
        <argsstring>(cy_http_server_t server_handle)</argsstring>
        <name>cy_http_server_start</name>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref></type>
          <declname>server_handle</declname>
        </param>
        <briefdescription>
<para>Starts a HTTP server daemon (web server) </para>        </briefdescription>
        <detaileddescription>
<para>The web server implements HTTP 1.1 using a non-blocking architecture which allows multiple sockets to be served simultaneously. Web pages and other files can be served either dynamically from a function or from static data in memory or internal/external flash resources. Prior to calling this API, API <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref> must be called for creating HTTP{ server instance.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP server handle created using <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="384" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga9883b5677f3f49d229d8883f414e9806" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_stop</definition>
        <argsstring>(cy_http_server_t server_handle)</argsstring>
        <name>cy_http_server_stop</name>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref></type>
          <declname>server_handle</declname>
        </param>
        <briefdescription>
<para>Stops a HTTP server daemon (web server) Before calling this API function, API <ref kindref="member" refid="group__group__c__api__functions_1ga3cb020a5e45f80b55ea3fc2eb1c6c3df">cy_http_server_start</ref> must be called to start HTTP server. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP server handle created using <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref>. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="393" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gad03c8e19c0792c4307356e52ba3006ee" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_register_resource</definition>
        <argsstring>(cy_http_server_t server_handle, uint8_t *url, uint8_t *mime_type, cy_url_resource_type url_resource_type, void *resource_data)</argsstring>
        <name>cy_http_server_register_resource</name>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref></type>
          <declname>server_handle</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>url</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>mime_type</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga32d601085911dbd59b7ecd50a6b7f584">cy_url_resource_type</ref></type>
          <declname>url_resource_type</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>resource_data</declname>
        </param>
        <briefdescription>
<para>Used to register a resource(static/dynamic) with the HTTP server. </para>        </briefdescription>
        <detaileddescription>
<para>All static resources must have been registered before calling <ref kindref="member" refid="group__group__c__api__functions_1ga3cb020a5e45f80b55ea3fc2eb1c6c3df">cy_http_server_start</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP server handle created using <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">url</parametername>
</parameternamelist>
<parameterdescription>
<para>: URL of the resource. The application should reserve memory for the URL. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">mime_type</parametername>
</parameternamelist>
<parameterdescription>
<para>: MIME type of the resource. The application should reserve memory for the MIME type. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">url_resource_type</parametername>
</parameternamelist>
<parameterdescription>
<para>: Content type of the resource. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">resource_data</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the static or dynamic resource type structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes in <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="407" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gaf4743c3b0e6ef9ba4afc5e1b5531691f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_enable_chunked_transfer</definition>
        <argsstring>(cy_http_response_stream_t *stream)</argsstring>
        <name>cy_http_server_response_stream_enable_chunked_transfer</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <briefdescription>
<para>Enables chunked transfer encoding on the HTTP stream. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the HTTP stream.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="416" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gab24071765e97d90225a33ead1318298e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_disable_chunked_transfer</definition>
        <argsstring>(cy_http_response_stream_t *stream)</argsstring>
        <name>cy_http_server_response_stream_disable_chunked_transfer</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <briefdescription>
<para>Disables chunked transfer encoding on the HTTP stream. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the HTTP stream.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="425" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gafb8df6439fdadb46d738ecd43cbaf002" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_write_header</definition>
        <argsstring>(cy_http_response_stream_t *stream, cy_http_status_codes_t status_code, uint32_t content_length, cy_http_cache_t cache_type, cy_http_mime_type_t mime_type)</argsstring>
        <name>cy_http_server_response_stream_write_header</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1gad1b0486d4ab51b5b1969e54aae1e5edd">cy_http_status_codes_t</ref></type>
          <declname>status_code</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>content_length</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga64b862dacedd67c3d74845c1a6038b89">cy_http_cache_t</ref></type>
          <declname>cache_type</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1gab6be7a35bfdbd62b763603d280dc4ee1">cy_http_mime_type_t</ref></type>
          <declname>mime_type</declname>
        </param>
        <briefdescription>
<para>Writes HTTP header to the HTTP stream provided. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the HTTP stream. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">status_code</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP status code. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">content_length</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP content length to follow, in bytes. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">cache_type</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP cache type (enabled or disabled). In AnyCloud and Mbed frameworks, the caching feature is not supported; therefore, this parameter should be always <ref kindref="member" refid="group__http__server__struct_1gga64b862dacedd67c3d74845c1a6038b89a21676ce46eff61339e33dded1daeaaea">CY_HTTP_CACHE_DISABLED</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">mime_type</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP MIME type.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="439" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga743dd1f394056a6ac09a6d4d8f17035c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_write_payload</definition>
        <argsstring>(cy_http_response_stream_t *stream, const void *data, uint32_t length)</argsstring>
        <name>cy_http_server_response_stream_write_payload</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <param>
          <type>const void *</type>
          <declname>data</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>length</declname>
        </param>
        <briefdescription>
<para>Writes data to the HTTP stream. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP stream to write the data into. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">data</parametername>
</parameternamelist>
<parameterdescription>
<para>: data to write. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">length</parametername>
</parameternamelist>
<parameterdescription>
<para>: data length in bytes.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="450" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga38608388fa4a8298375bec0b3a74c28f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_write_resource</definition>
        <argsstring>(cy_http_response_stream_t *stream, const void *resource)</argsstring>
        <name>cy_http_server_response_stream_write_resource</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <param>
          <type>const void *</type>
          <declname>resource</declname>
        </param>
        <briefdescription>
<para>Writes resource to HTTP stream. </para>        </briefdescription>
        <detaileddescription>
<para>Currently not supported in AnyCloud and Mbed frameworks.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP stream to write the resource into. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">resource</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to resource.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="461" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga42ed7afac50f1e923520e6ef6f3c0098" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_flush</definition>
        <argsstring>(cy_http_response_stream_t *stream)</argsstring>
        <name>cy_http_server_response_stream_flush</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <briefdescription>
<para>Flushes the HTTP stream. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP stream to flush.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="470" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gaf051d4ad577610bdbd38fa6aa8297592" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_disconnect</definition>
        <argsstring>(cy_http_response_stream_t *stream)</argsstring>
        <name>cy_http_server_response_stream_disconnect</name>
        <param>
          <type><ref kindref="compound" refid="structcy__http__response__stream__t">cy_http_response_stream_t</ref> *</type>
          <declname>stream</declname>
        </param>
        <briefdescription>
<para>Queues a disconnect request to the HTTP server. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">stream</parametername>
</parameternamelist>
<parameterdescription>
<para>: Pointer to the HTTP stream.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="479" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gaf6442a83a82a102c4490280d85e0ef88" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_response_stream_disconnect_all</definition>
        <argsstring>(cy_http_server_t server_handle)</argsstring>
        <name>cy_http_server_response_stream_disconnect_all</name>
        <param>
          <type><ref kindref="member" refid="group__http__server__struct_1ga047ac1c7063077c58da4cc991ad3b1bf">cy_http_server_t</ref></type>
          <declname>server_handle</declname>
        </param>
        <briefdescription>
<para>Disconnects all the HTTP streams associated with the given server. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">server_handle</parametername>
</parameternamelist>
<parameterdescription>
<para>: HTTP server handle created using <ref kindref="member" refid="group__group__c__api__functions_1ga1ca51f61222e3aed87c0a15955e2e269">cy_http_server_create</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="488" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gaf2bee362096d822f39a4851c63c5abf3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_get_query_parameter_value</definition>
        <argsstring>(const char *url_query, const char *parameter_key, char **parameter_value, uint32_t *value_length)</argsstring>
        <name>cy_http_server_get_query_parameter_value</name>
        <param>
          <type>const char *</type>
          <declname>url_query</declname>
        </param>
        <param>
          <type>const char *</type>
          <declname>parameter_key</declname>
        </param>
        <param>
          <type>char **</type>
          <declname>parameter_value</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>value_length</declname>
        </param>
        <briefdescription>
<para>Searches for a parameter (key-value pair) in a URL query string and returns a pointer to the value. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">url_query</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL-terminated URL query string. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">parameter_key</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL-terminated Key or name of the parameter to find in the URL query string. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">parameter_value</parametername>
</parameternamelist>
<parameterdescription>
<para>: If the parameter with the given key is found, this pointer will point to the parameter value upon return; NULL otherwise. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">value_length</parametername>
</parameternamelist>
<parameterdescription>
<para>: This variable will contain the length of the parameter value upon return; 0 otherwise.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS if found; CY_RSLT_NOT_FOUND otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="500" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1ga3825981f26c68c82d781f5fb7f2a4bfc" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_get_query_parameter_count</definition>
        <argsstring>(const char *url_query, uint32_t *count)</argsstring>
        <name>cy_http_server_get_query_parameter_count</name>
        <param>
          <type>const char *</type>
          <declname>url_query</declname>
        </param>
        <param>
          <type>uint32_t *</type>
          <declname>count</declname>
        </param>
        <briefdescription>
<para>Returns the number of parameters found in the URL query string. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">url_query</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL terminated URL query string. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">count</parametername>
</parameternamelist>
<parameterdescription>
<para>: Parameter count.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS on success; error codes from <ref kindref="compound" refid="group__http__server__defines">Macros</ref> otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="510" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__c__api__functions_1gac1cd7f7af7f2f9aa6e288be4e2a6419f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_http_server_match_query_parameter</definition>
        <argsstring>(const char *url_query, const char *parameter_key, const char *parameter_value)</argsstring>
        <name>cy_http_server_match_query_parameter</name>
        <param>
          <type>const char *</type>
          <declname>url_query</declname>
        </param>
        <param>
          <type>const char *</type>
          <declname>parameter_key</declname>
        </param>
        <param>
          <type>const char *</type>
          <declname>parameter_value</declname>
        </param>
        <briefdescription>
<para>Checks whether the given parameter key-value pair is present in the given URL query. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">url_query</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL-terminated URL query string. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">parameter_key</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL-terminated key or name of the parameter to find in the URL query string. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">parameter_value</parametername>
</parameternamelist>
<parameterdescription>
<para>: NULL-terminated value of the parameter to find in the URL query string.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_rslt_t : CY_RSLT_SUCCESS if matched; CY_RSLT_NOT_FOUND if matching parameter is not found. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/mw-http-server/include/cy_http_server.h" line="521" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>C API provided by HTTP server library. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>