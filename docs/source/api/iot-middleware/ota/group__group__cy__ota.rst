===============================
Cypress Over The Air (OTA) API
===============================

.. doxygengroup:: group_cy_ota
   :project: ota-api

.. toctree::

   group__group__ota__config.rst
   group__group__ota__macros.rst
   group__group__ota__typedefs.rst
   group__group__ota__callback.rst
   group__group__ota__structures.rst
   group__group__ota__functions.rst


