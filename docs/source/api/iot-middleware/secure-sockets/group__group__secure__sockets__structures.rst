Structures
============

.. doxygengroup:: group_secure_sockets_structures
   :project: secure-sockets	
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   