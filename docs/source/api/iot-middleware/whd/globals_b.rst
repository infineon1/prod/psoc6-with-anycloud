===
b
===

Here is a list of all documented functions, variables, defines, enums,
and typedefs with links to the documentation:

.. rubric:: - b -
   :name: b--

-  BDC_HEADER_OFFSET_TO_DATA :
   `whd_types.h <whd__types_8h.html#a559655385d80ca97754fafa29ac87717>`__
-  BDC_HEADER_WITH_PAD :
   `whd_types.h <whd__types_8h.html#a50f830e25a5ac6d7d0e3eb8d23e90943>`__
-  BLOCK_SIZE :
   `whd_resource_api.h <whd__resource__api_8h.html#ad51ded0bbd705f02f73fc60c0b721ced>`__
-  BUFFER_OVERHEAD :
   `whd_types.h <whd__types_8h.html#a9b159bb86ca28f2200f30661441f232e>`__
-  BUS_READ :
   `whd_types.h <whd__types_8h.html#afc8e8073d434bf124933526a8184313aa6c6521c79c32270c3d3fe237334e79a8>`__
-  BUS_WRITE :
   `whd_types.h <whd__types_8h.html#afc8e8073d434bf124933526a8184313aa1647c751f170186c40770f46b6353ef9>`__

