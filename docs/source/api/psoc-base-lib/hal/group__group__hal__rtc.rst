======================
RTC (Real-Time Clock)
======================

.. doxygengroup:: group_hal_rtc
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__rtc.rst
   

