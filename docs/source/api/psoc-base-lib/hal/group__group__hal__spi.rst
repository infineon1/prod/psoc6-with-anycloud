==================================
SPI (Serial Peripheral Interface)
==================================

.. doxygengroup:: group_hal_spi
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__spi.rst
   

