====================================================
UART (Universal Asynchronous Receiver-Transmitter)
====================================================

.. doxygengroup:: group_hal_uart
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__uart.rst
   

