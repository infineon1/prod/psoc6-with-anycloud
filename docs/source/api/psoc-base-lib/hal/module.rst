==================
HAL API Reference
==================
.. toctree::
   :hidden:

   group__group__hal__types.rst
   group__group__hal.rst
   group__group__hal__impl.rst
   

   

The following provides a list of HAL API documentation

    
         +-----------------------------------+-----------------------------------+
         | `HAL General                      | This section documents the basic  |
         | Types/Macros <group__group        | types and macros that are used by |
         | __hal__types.html>`__             | multiple HAL drivers              |
         +-----------------------------------+-----------------------------------+
         | `Result                           | Defines a type and related        |
         | Type <group__group                | utilities for function result     |        
         | __result.html>`__                 |                                   |
         +-----------------------------------+-----------------------------------+
         | `General                          | This section documents the basic  |
         | Types <group__gr                  | types that are used by multiple   |
         | oup__hal__general__types.html>`__ | HAL drivers                       |         
         +-----------------------------------+-----------------------------------+
         | `Result                           | HAL specific return codes         |
         | Codes <gr                         | definitions for all drivers       |        
         | oup__group__hal__results.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `ADC HAL                          | ADC specific return codes         |
         | Results <group__                  |                                   |       
         | group__hal__results__adc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Clock HAL                        | Clock specific return codes       |
         | Results <group__gr                |                                   |       
         | oup__hal__results__clock.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Comparator HAL                   | Comparator specific return codes  |
         | Results <group__g                 |                                   |         
         | roup__hal__results__comp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `CRC HAL                          | CRC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__crc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `DAC HAL                          | DAC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__dac.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `DMA HAL                          | DMA specific return codes         |
         | Results <group__                  |                                   |                               
         | group__hal__results__dma.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `EZI2C HAL                        | EZI2C specific return codes       |
         | Results <group__gr                |                                   |                               
         | oup__hal__results__ezi2c.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Flash HAL                        | Flash specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__flash.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `HWMGR HAL                        | HWMGR specific return codes       |
         | Results <group__gr                |                                   |         
         | oup__hal__results__hwmgr.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2C HAL                          | I2C specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__i2c.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2S HAL                          | I2S specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__i2s.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Interconnect HAL                 | Interconnect specific return      |
         | Results <group__group__ha         | codes                             | 
         | l__results__interconnect.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `LPTimer HAL                      | LPTimer specific return codes     |
         | Results <group__grou              |                                   |
         | p__hal__results__lptimer.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Opamp HAL                        | Opamp specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__opamp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PDM/PCM HAL                      | PDM/PCM specific return codes     |
         | Results <group__gro               |                                   |
         | up__hal__results__pdmpcm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PWM HAL                          | PWM specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__pwm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `QSPI HAL                         | QSPI specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__qspi.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Quadrature Decoder HAL           | Quadrature Decoder specific       |
         | Results <group__grou              | return codes                      |
         | p__hal__results__quaddec.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `RTC HAL                          | RTC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__rtc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SDHC HAL                         | SDHC specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__sdhc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SDIO HAL                         | SDIO specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__sdio.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SPI HAL                          | SPI specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__spi.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SYSPM HAL                        | SYSPM specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__syspm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Timer HAL                        | Timer specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__timer.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `TRNG HAL                         | TRNG specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__trng.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `UART HAL                         | UART specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__uart.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `USB Device HAL                   | USB Device specific return codes  |
         | Results <group__gro               |                                   |
         | up__hal__results__usbdev.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `WDT HAL                          | WDT specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__wdt.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `System Power                     | Interface for changing power      |
         | Management <group__group          | states and restricting when they  |
         | __hal__syspm.html>`__             |                                   |
         +-----------------------------------+-----------------------------------+
         | `SYSPM HAL                        | SYSPM specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__syspm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Clock <group__group              | Interface for getting and         |
         | __hal__clock.html>`__             | changing clock configuration      |
         +-----------------------------------+-----------------------------------+
         | `Clock HAL                        | Clock specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__clock.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Group_hal_tolerance <grou        |                                   |         
         | p__group__hal__tolerance.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Overrideable                     | These macros can be defined to a  |
         | Macros <gro                       | custom value globally to modify   |
         | up__group__hal__override.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Implementation Specific          | The following types are used by   |
         | Types <group__group__ha           | the HAL, but are defined by the   |
         | l__types__implementation.html>`__ | implementation                    |
         +-----------------------------------+-----------------------------------+
         | `HAL                              | This section documents the        |
         | Drivers <group__group             | drivers which form the stable API |
         | __hal.html>`__                    | of the Cypress HAL                |
         +-----------------------------------+-----------------------------------+
         | `DMA (Direct Memory               | High level interface for          |
         | Access) <group__group             | interacting with the direct       |
         | __hal__dma.html>`__               |                                   |
         +-----------------------------------+-----------------------------------+
         | `DMA HAL                          | DMA specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__dma.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `ADC (Analog to Digital           | High level interface for          |
         | Converter) <group__group          | interacting with the analog to    |
         | __hal__adc.html>`__               | digital converter (ADC)           |
         +-----------------------------------+-----------------------------------+
         | `ADC HAL                          | ADC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__adc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Clock <group__group__            | Interface for getting and         |
         | hal__clock.html>`__               | changing clock configuration      |
         +-----------------------------------+-----------------------------------+
         | `Clock HAL                        | Clock specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__clock.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Group_hal_tolerance <grou        |                                   |
         | p__group__hal__tolerance.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `COMP (Analog                     | High level interface for          |
         | Comparator) <group__group         | interacting with an analog        |
         | __hal__comp.html>`__              | Comparator                        |          
         +-----------------------------------+-----------------------------------+
         | `Comparator HAL                   | Comparator specific return codes  |
         | Results <group__g                 |                                   |
         | roup__hal__results__comp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `CRC (Cyclic Redundancy           | High level interface for          |
         | Check) <group__group              | interacting with the CRC, which   |
         | __hal__crc.html>`__               | provides hardware accelerated CRC |
         |                                   | computations                      |
         +-----------------------------------+-----------------------------------+
         | `CRC HAL                          | CRC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__crc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `DAC (Digital to Analog           | High level interface for          |
         | Converter) <group__group          | interacting with the digital to   |
         | __hal__dac.html>`__               | analog converter (DAC)            |
         +-----------------------------------+-----------------------------------+
         | `DAC HAL                          | DAC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__dac.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `EZI2C (Inter-Integrated          | High level interface for          |
         | Circuit) <group__group            | interacting with the Cypress EZ   |
         | __hal__ezi2c.html>`__             | Inter-Integrated Circuit (EZI2C)  |
         +-----------------------------------+-----------------------------------+
         | `EZI2C HAL                        | EZI2C specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__ezi2c.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Flash (Flash System              | High level interface to the       |
         | Routine) <group__group            | internal flash memory             |
         | __hal__flash.html>`__             |                                   |
         +-----------------------------------+-----------------------------------+
         | `Flash HAL                        | Flash specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__flash.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `System Power                     | Interface for changing power      |
         | Management <group__group          | states and restricting when they  |
         | __hal__syspm.html>`__             | are allowed                       |
         +-----------------------------------+-----------------------------------+
         | `SYSPM HAL                        | SYSPM specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__syspm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `GPIO (General Purpose Input      | High level interface for          |
         | Output) <group__                  | configuring and interacting with  |
         | group__hal__gpio.html>`__         | general purpose input/outputs     |
         |                                   | (GPIO)                            |
         +-----------------------------------+-----------------------------------+
         | `HWMGR (Hardware                  | High level interface to the       |
         | Manager) <group__group            | Hardware Manager                  |
         | __hal__hwmgr.html>`__             |                                   |
         +-----------------------------------+-----------------------------------+
         | `HWMGR HAL                        | HWMGR specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__hwmgr.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2C (Inter-Integrated            | High level interface for          |
         | Circuit) <group__group            | interacting with the I2C resource |
         | __hal__i2c.html>`__               |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2C HAL                          | I2C specific return codes         |
         | Res ults <group__                 |                                   |
         | group__hal__results__i2c.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2S (Inter-IC                    | High level interface for          |
         | Sound) <group__group              | interacting with the Inter-IC     |
         | __hal__i2s.html>`__               | Sound (I2S)                       |        
         +-----------------------------------+-----------------------------------+
         | `I2S HAL                          | I2S specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__i2s.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `INTERCONNECT (Internal           | High level interface to the       |
         | digital routing) <group__         | Cypress digital routing           |
         | group__hal__interconnect.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Interconnect HAL                 | Interconnect specific return      |
         | Results <group__group__ha         | codes                             |
         | l__results__interconnect.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `LPTimer (Low-Power               | High level interface for          |
         | Timer) <gr                        | interacting with the low-power    |
         | oup__group__hal__lptimer.html>`__ | timer (LPTimer)                   |
         +-----------------------------------+-----------------------------------+
         | `LPTimer HAL                      | LPTimer specific return codes     |
         | Results <group__grou              |                                   |
         | p__hal__results__lptimer.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Opamp (Operational               | High level interface for          |
         | Amplifier) <group__               | interacting with the Operational  |
         | group__hal__opamp.html>`__        | Amplifier (Opamp)                 |
         +-----------------------------------+-----------------------------------+
         | `Opamp HAL                        | Opamp specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__opamp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PDM/PCM (Pulse-Density           | High level interface for          |
         | Modulation to Pulse-Code          | interacting with the              |
         | Modulation                        | pulse-density modulation to       |
         | Converter) <g                     | pulse-code modulation (PDM/PCM)   |
         | roup__group__hal__pdmpcm.html>`__ | converter                         |
         +-----------------------------------+-----------------------------------+
         | `PDM/PCM HAL                      | PDM/PCM specific return codes     |
         | Results <group__gro               |                                   |
         | up__hal__results__pdmpcm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PWM (Pulse Width                 | High level interface for          |
         | Modulator) <group__group__        | interacting with the pulse width  |
         | hal__pwm.html>`__                 | modulator (PWM) hardware resource |
         +-----------------------------------+-----------------------------------+
         | `PWM HAL                          | PWM specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__pwm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `QSPI (Quad Serial Peripheral     | High level interface for          |
         | Interface) <group__group__        | interacting with the Quad-SPI     |
         | hal__qspi.html>`__                | interface                         |
         +-----------------------------------+-----------------------------------+
         | `QSPI HAL                         | QSPI specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__qspi.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Quadrature                       | High level interface for          |
         | Decoder <gr                       | interacting with the Quadrature   |
         | oup__group__hal__quaddec.html>`__ | Decoder hardware resource         |
         +-----------------------------------+-----------------------------------+
         | `Quadrature Decoder HAL           | Quadrature Decoder specific       |
         | Results <group__grou              | return codes                      |
         | p__hal__results__quaddec.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `RTC (Real-Time                   | High level interface for          |
         | Clock) <group__group__            | interacting with the real-time    |
         | hal__rtc.html>`__                 | clock (RTC)                       |
         +-----------------------------------+-----------------------------------+
         | `RTC HAL                          | RTC specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__rtc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SDHC (SD Host                    | High level interface to the       |
         | Controller) <group__              | Secure Digital Host Controller    |
         | group__hal__sdhc.html>`__         | (SDHC)                            |
         +-----------------------------------+-----------------------------------+
         | `SDHC HAL                         | SDHC specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__sdhc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SDIO (Secure Digital Input       | High level interface to the       |
         | Output) <group__                  | Secure Digital Input Output       |
         | group__hal__sdio.html>`__         | (SDIO)                            |
         +-----------------------------------+-----------------------------------+
         | `SDIO HAL                         | SDIO specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__sdio.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `SPI (Serial Peripheral           | High level interface for          |
         | Interface) <group__               | interacting with the Serial       |
         | group__hal__spi.html>`__          | Peripheral Interface (SPI)        |
         +-----------------------------------+-----------------------------------+
         | `SPI HAL                          | SPI specific return codes         |
         | Results <group__                  |                                   |
         | group__hal__results__spi.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `System <g                        | High level interface for          |
         | roup__group__hal__system.html>`__ | interacting with reset and delays |
         +-----------------------------------+-----------------------------------+
         | `Timer                            | High level interface for          |
         | (Timer/Counter) <group__          | interacting with the              |
         | group__hal__timer.html>`__        | Timer/Counter hardware resource   |
         +-----------------------------------+-----------------------------------+
         | `Timer HAL                        | Timer specific return codes       |
         | Results <group__gr                |                                   |
         | oup__hal__results__timer.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `TRNG (True Random Number         | High level interface to the True  |
         | Generator) <group__               | Random Number Generator (TRNG)    |
         | group__hal__trng.html>`__         |                                   |
         +-----------------------------------+-----------------------------------+
         | `TRNG HAL                         | TRNG specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__trng.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `UART (Universal Asynchronous     | High level interface for          |
         | Receiver-                         | interacting with the Universal    |
         | Transmitter) <group__             | Asynchronous Receiver-Transmitter |
         | group__hal__uart.html>`__         | (UART)                            |
         +-----------------------------------+-----------------------------------+
         | `UART HAL                         | UART specific return codes        |
         | Results <group__g                 |                                   |
         | roup__hal__results__uart.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `USB                              | High level interface for          |
         | Device <gro                       | interacting with the USB Device   |
         | up__group__hal__usb__dev.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `USB Device HAL                   | USB Device specific return codes  |
         | Results <group__gro               |                                   |
         | up__hal__results__usbdev.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Endpoint <group__group_          | APIs relating to endpoint         |
         | _hal__usb__dev__endpoint.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `Common <group__grou              |                                   |
         | p__hal__usb__dev__common.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `EP0 <group__group                | APIs relating specifically to     |
         | __hal__usb__dev__ep0.html>`__     | management of endpoint zero       |
         +-----------------------------------+-----------------------------------+
         | `WDT (Watchdog                    | High level interface to the       |
         | Timer) <group__                   | Watchdog Timer (WDT)              |
         | group__hal__wdt.html>`__          |                                   |
         +-----------------------------------+-----------------------------------+
         | `WDT HAL                          | WDT specific return codes         |
         | Res ults <group__                 |                                   |
         | group__hal__results__wdt.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC 6 Implementation            | This section provides details     |
         | Specific <group__                 | about the PSoC 6 implementation   |
         | group__hal__impl.html>`__         | of the Cypress HAL                |
         +-----------------------------------+-----------------------------------+
         | `Clocks <group_                   | Implementation specific interface |
         | _group__hal__impl__clock.html>`__ | for using the Clock driver        |
         +-----------------------------------+-----------------------------------+
         | `COMP (CTB                        | Implementation of the analog      |
         | Opamps) <grou                     | comparator (COMP) driver on top   |
         | p__group__hal__comp__ctb.html>`__ | of the CTB opamps                 |
         +-----------------------------------+-----------------------------------+
         | `COMP (LP Comparator              | Implementation of the analog      |
         | block) <gro                       | comparator (COMP) driver on top   |
         | up__group__hal__comp__lp.html>`__ | of the LP (low power) comparator  |
         +-----------------------------------+-----------------------------------+
         | `Deprecated <group__grou          | The following PSoC 6 specific     |
         | p__hal__impl__deprecated.html>`__ | items have been deprecated and    |
         |                                   | replaced by more generic items    |
         +-----------------------------------+-----------------------------------+
         | `DMA (Direct Memory               | DW (DataWire) is one of two DMA   |
         | Access) <grou                     | hardware implementations for      |
         | p__group__hal__impl__dma.html>`__ | PSOC6                             |
         +-----------------------------------+-----------------------------------+
         | `DMAC (Direct Memory Access       | Implementation specific interface |
         | Controller                        | for using the DMAC DMA peripheral |
         | ) <group__gro                     |                                   |
         | up__hal__impl__dma__dmac.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `DW                               | Implementation specific interface |
         | (Datawire) <group__g              | for using the Datawire DMA        |
         | roup__hal__impl__dma__dw.html>`__ | peripheral                        |
         +-----------------------------------+-----------------------------------+
         | `PSoC 6 Specific Hardware         | Aliases for types which are part  |
         | Types <group__gro                 | of the public HAL interface but   |
         | up__hal__impl__hw__types.html>`__ | whose representations need to     |
         +-----------------------------------+-----------------------------------+
         | `Pins <group__group_              | Definitions for the pinout for    |
         | _hal__impl__pin__package.html>`__ | each supported device             |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 104-M-CSP-BLE <group__gr          | specific to the PSoC6_01          |
         | oup__hal__impl__pin__package__pso | 104-M-CSP-BLE package             |
         | c6__01__104__m__csp__ble.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 104-M-CSP-BLE-USB <group__group__ | specific to the PSoC6_01          |
         | hal__impl__pin__package__psoc6__0 | 104-M-CSP-BLE-USB package         |
         | 1__104__m__csp__ble__usb.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 116-BGA-BLE <group_               | specific to the PSoC6_01          |
         | _group__hal__impl__pin__package__ | 116-BGA-BLE package               |
         | psoc6__01__116__bga__ble.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 116-BGA-USB <group_               | specific to the PSoC6_01          |
         | _group__hal__impl__pin__package__ | 116-BGA-USB package               |
         | psoc6__01__116__bga__usb.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 124-BGA <g                        | specific to the PSoC6_01 124-BGA  |
         | roup__group__hal__impl__pin__pack | package                           |
         | age__psoc6__01__124__bga.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 124-BGA-SIP <group__group         | specific to the PSoC6_01          |
         | __hal__impl__pin__package__       | 124-BGA-SIP package               |
         | psoc6__01__124__bga__sip.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 43-SMT <group__                   | specific to the PSoC6_01 43-SMT   |
         | group__hal__impl__pin__pac        | package                           |
         | kage__psoc6__01__43__smt.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 68-QFN-BLE <group                 | specific to the PSoC6_01          |
         | __group__hal__impl__pin__package_ | 68-QFN-BLE package                |
         | _psoc6__01__68__qfn__ble.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_01                         | Pin definitions and connections   |
         | 80-WLCSP <gr                      | specific to the PSoC6_01 80-WLCSP |
         | oup__group__hal__impl__pin__packa | package                           |
         | ge__psoc6__01__80__wlcsp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_02                         | Pin definitions and connections   |
         | 100-WLCSP <gro                    | specific to the PSoC6_02          |
         | up__group__hal__impl__pin__packag | 100-WLCSP package                 |
         | e__psoc6__02__100__wlcsp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_02                         | Pin definitions and connections   |
         | 124-BGA <g                        | specific to the PSoC6_02 124-BGA  |
         | roup__group__hal__impl__pin__pack | package                           |
         | age__psoc6__02__124__bga.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_02                         | Pin definitions and connections   |
         | 128-TQFP <gr                      | specific to the PSoC6_02 128-TQFP |
         | oup__group__hal__impl__pin__packa | package                           |
         | ge__psoc6__02__128__tqfp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_02                         | Pin definitions and connections   |
         | 68-QFN <group__                   | specific to the PSoC6_02 68-QFN   |
         | group__hal__impl__pin__pac        | package                           |
         | kage__psoc6__02__68__qfn.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_03                         | Pin definitions and connections   |
         | 100-TQFP <gr                      | specific to the PSoC6_03 100-TQFP |
         | oup__group__hal__impl__pin__packa | package                           |
         | ge__psoc6__03__100__tqfp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_03                         | Pin definitions and connections   |
         | 49-WLCSP <gr                      | specific to the PSoC6_03 49-WLCSP |
         | oup__group__hal__impl__pin__packa | package                           |
         | ge__psoc6__03__49__wlcsp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_03                         | Pin definitions and connections   |
         | 68-QFN <group__                   | specific to the PSoC6_03 68-QFN   |
         | group__hal__impl__pin__pac        | package                           |
         | kage__psoc6__03__68__qfn.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_04                         | Pin definitions and connections   |
         | 64-TQFP-                          | specific to the PSoC6_04          |
         | EPAD <group__                     | 64-TQFP-EPAD package              |
         | group__hal__impl__pin__package__p |                                   |
         | soc6__04__64__tqfp__epad.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_04                         | Pin definitions and connections   |
         | 68-QFN <group__                   | specific to the PSoC6_04 68-QFN   |
         | group__hal__impl__pin__pac        | package                           |
         | kage__psoc6__04__68__qfn.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSoC6_04                         | Pin definitions and connections   |
         | 80-TQFP <g                        | specific to the PSoC6_04 80-TQFP  |
         | roup__group__hal__impl__pin__pack | package                           |
         | age__psoc6__04__80__tqfp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `System Power                     | The PSoC 6 Power Management has   |
         | Management <group_                | the following characteristics:    |
         | _group__hal__impl__syspm.html>`__ | `CYHAL_SYSPM_SYSTE                |
         |                                   | M_NORMAL <gro                     |
         |                                   | up__group__hal__syspm.html#gga144 |
         |                                   | e474d3bfb544b207f172d1d8bd633acd2 |
         |                                   | a5522b6591eb77418369e464a3eb1>`__ |
         |                                   | equates to the Low Power mode     |
         |                                   | `CYHAL_SYSPM_SY                   |
         |                                   | STEM_LOW <gro                     |
         |                                   | up__group__hal__syspm.html#gga144 |
         |                                   | e474d3bfb544b207f172d1d8bd633af40 |
         |                                   | 60b98561e43a6b930576644af014f>`__ |
         |                                   | equates to the Ultra Low Power    |
         |                                   | mode                              |
         +-----------------------------------+-----------------------------------+
         | `Timer (Timer/Counter) <group__   |                                   |
         | group__hal__impl__timer.html>`__  |                                   |
         +-----------------------------------+-----------------------------------+
         | `Triggers <group__gr              | Trigger connections for supported |
         | oup__hal__impl__triggers.html>`__ | device families                   |
         +-----------------------------------+-----------------------------------+
         | `PSOC6_01 <group__group__hal__i   | Trigger connections for psoc6_01  |
         | mpl__triggers__psoc6__01.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSOC6_02 <group__                | Trigger connections for psoc6_02  |
         | group__hal__i                     |                                   |                                             
         | mpl__triggers__psoc6__02.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSOC6_03 <group__group__hal__i   | Trigger connections for psoc6_03  |
         | mpl__triggers__psoc6__03.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PSOC6_04 <group__group__hal__i   | Trigger connections for psoc6_04  |
         | mpl__triggers__psoc6__04.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `WDT (Watchdog                    | The PSoC 6 WDT is only capable of |
         | Timer) <grou                      | supporting certain timeout ranges |
         | p__group__hal__impl__wdt.html>`__ | below its maximum timeout         |
         +-----------------------------------+-----------------------------------+
         | `ADC (Analog Digital              |                                   |
         | Converter) <grou                  |                                   |
         | p__group__hal__impl__adc.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `COMP (Analog                     | On PSoC 6, the COMP driver can    |
         | Comparator) <group                | use either of two underlying      |
         | __group__hal__impl__comp.html>`__ | hardware blocks:                  |
         +-----------------------------------+-----------------------------------+
         | `DAC <grou                        |                                   |
         | p__group__hal__impl__dac.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `I2S (Inter-IC                    | The PSoC 6 I2S Supports the       |
         | Sound) <grou                      | following values for word and     |
         | p__group__hal__impl__i2s.html>`__ | channel lengths (with the         |
         |                                   | constraint that word length must  |
         |                                   | be less than or equal to channel  |
         +-----------------------------------+-----------------------------------+
         | `LPTI                             | The maximum number of ticks that  |
         | MER <group__g                     | can set to an LPTIMER is          |
         | roup__hal__impl__lptimer.html>`__ | 0xFFF0FFFF                        |
         +-----------------------------------+-----------------------------------+
         | `Opamp <group_                    |                                   |
         | _group__hal__impl__opamp.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PDM/PCM (Pulse Density           | The PSoC 6 PDM/PCM Supports the   |
         | Modulation to Pulse Code          | following conversion parameters:  |
         | Modulation                        |                                   |
         | Converter) <group__               |                                   |
         | group__hal__impl__pdmpcm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `PWM (Pulse Width                 |                                   |
         | Modulator) <grou                  |                                   |
         | p__group__hal__impl__pwm.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `QuadDec (Quadrature              |                                   |
         | Decoder) <group__g                |                                   |
         | roup__hal__impl__quaddec.html>`__ |                                   |
         +-----------------------------------+-----------------------------------+
         | `RTC (Real Time                   | Internally the PSoC 6 RTC only    |
         | Clock) <grou                      | stores the year as a two digit    |
         | p__group__hal__impl__rtc.html>`__ | BCD value (0-99); no century      |
         |                                   | information is stored             |
         +-----------------------------------+-----------------------------------+
         | `UDB SDIO (Secure Digital         | The UDB based SDIO interface      |
         | Input Output)  <group__gro        | allows for communicating between  |
         | up__hal__impl__udb__sdio.html>`__ | a PSoC 6 and a Cypress wireless   |
         |                                   | device such as the CYW4343W,      |
         |                                   | CYW43438, or CYW43012             |
         +-----------------------------------+-----------------------------------+         


