<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__trng_8c" kind="file" language="C++">
    <compoundname>cyhal_trng.c</compoundname>
    <includes local="yes" refid="cyhal__hwmgr_8h">cyhal_hwmgr.h</includes>
    <includes local="yes" refid="cyhal__crypto__common_8h">cyhal_crypto_common.h</includes>
    <includes local="yes" refid="cyhal__trng__impl_8h">cyhal_trng_impl.h</includes>
    <includes local="yes">cy_utils.h</includes>
    <incdepgraph>
      <node id="420">
        <label>cyhal_trng.c</label>
        <link refid="cyhal__trng_8c" />
        <childnode refid="421" relation="include">
        </childnode>
      </node>
      <node id="421">
        <label>cy_utils.h</label>
      </node>
    </incdepgraph>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="2"><highlight class="comment">*<sp />File<sp />Name:<sp />cyhal_trng.c</highlight></codeline>
<codeline lineno="3"><highlight class="comment">*</highlight></codeline>
<codeline lineno="4"><highlight class="comment">*<sp />Description:</highlight></codeline>
<codeline lineno="5"><highlight class="comment">*<sp />Provides<sp />a<sp />high<sp />level<sp />interface<sp />for<sp />interacting<sp />with<sp />the<sp />Cypress<sp />True<sp />Random</highlight></codeline>
<codeline lineno="6"><highlight class="comment">*<sp />Number<sp />Generator.<sp />This<sp />is<sp />a<sp />wrapper<sp />around<sp />the<sp />lower<sp />level<sp />PDL<sp />API.</highlight></codeline>
<codeline lineno="7"><highlight class="comment">*</highlight></codeline>
<codeline lineno="8"><highlight class="comment">********************************************************************************</highlight></codeline>
<codeline lineno="9"><highlight class="comment">*<sp />\copyright</highlight></codeline>
<codeline lineno="10"><highlight class="comment">*<sp />Copyright<sp />2018-2020<sp />Cypress<sp />Semiconductor<sp />Corporation</highlight></codeline>
<codeline lineno="11"><highlight class="comment">*<sp />SPDX-License-Identifier:<sp />Apache-2.0</highlight></codeline>
<codeline lineno="12"><highlight class="comment">*</highlight></codeline>
<codeline lineno="13"><highlight class="comment">*<sp />Licensed<sp />under<sp />the<sp />Apache<sp />License,<sp />Version<sp />2.0<sp />(the<sp />"License");</highlight></codeline>
<codeline lineno="14"><highlight class="comment">*<sp />you<sp />may<sp />not<sp />use<sp />this<sp />file<sp />except<sp />in<sp />compliance<sp />with<sp />the<sp />License.</highlight></codeline>
<codeline lineno="15"><highlight class="comment">*<sp />You<sp />may<sp />obtain<sp />a<sp />copy<sp />of<sp />the<sp />License<sp />at</highlight></codeline>
<codeline lineno="16"><highlight class="comment">*</highlight></codeline>
<codeline lineno="17"><highlight class="comment">*<sp /><sp /><sp /><sp /><sp />http://www.apache.org/licenses/LICENSE-2.0</highlight></codeline>
<codeline lineno="18"><highlight class="comment">*</highlight></codeline>
<codeline lineno="19"><highlight class="comment">*<sp />Unless<sp />required<sp />by<sp />applicable<sp />law<sp />or<sp />agreed<sp />to<sp />in<sp />writing,<sp />software</highlight></codeline>
<codeline lineno="20"><highlight class="comment">*<sp />distributed<sp />under<sp />the<sp />License<sp />is<sp />distributed<sp />on<sp />an<sp />"AS<sp />IS"<sp />BASIS,</highlight></codeline>
<codeline lineno="21"><highlight class="comment">*<sp />WITHOUT<sp />WARRANTIES<sp />OR<sp />CONDITIONS<sp />OF<sp />ANY<sp />KIND,<sp />either<sp />express<sp />or<sp />implied.</highlight></codeline>
<codeline lineno="22"><highlight class="comment">*<sp />See<sp />the<sp />License<sp />for<sp />the<sp />specific<sp />language<sp />governing<sp />permissions<sp />and</highlight></codeline>
<codeline lineno="23"><highlight class="comment">*<sp />limitations<sp />under<sp />the<sp />License.</highlight></codeline>
<codeline lineno="24"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="25"><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_hwmgr.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_crypto_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_trng_impl.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_utils.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal">{</highlight></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="39"><highlight class="comment">*<sp /><sp /><sp /><sp /><sp /><sp /><sp />Functions</highlight></codeline>
<codeline lineno="40"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp /><ref kindref="member" refid="group__group__hal__trng_1ga499f730a1bf8a19bbd685bb40e7a0887">cyhal_trng_init</ref>(<ref kindref="compound" refid="structcyhal__trng__t">cyhal_trng_t</ref><sp />*obj)</highlight></codeline>
<codeline lineno="42"><highlight class="normal">{</highlight></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />cyhal_crypto_reserve(&amp;(obj-&gt;base),<sp />&amp;(obj-&gt;resource),<sp />CYHAL_CRYPTO_TRNG);</highlight></codeline>
<codeline lineno="45"><highlight class="normal">}</highlight></codeline>
<codeline lineno="46"><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__hal__trng_1gaf11f2e6c4841065309ecfeab50f488eb">cyhal_trng_free</ref>(<ref kindref="compound" refid="structcyhal__trng__t">cyhal_trng_t</ref><sp />*obj)</highlight></codeline>
<codeline lineno="48"><highlight class="normal">{</highlight></codeline>
<codeline lineno="49"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj<sp />||<sp />obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref><sp />!=<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975ba25fa2cb684b519d6826067ce6d5d1afe">CYHAL_RSC_CRYPTO</ref>);</highlight></codeline>
<codeline lineno="50"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref><sp />!=<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975baf4c05e33bb994cc6a1d94bd301dcc988">CYHAL_RSC_INVALID</ref>)</highlight></codeline>
<codeline lineno="51"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="52"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_crypto_free(obj-&gt;base,<sp />&amp;(obj-&gt;resource),<sp />CYHAL_CRYPTO_TRNG);</highlight></codeline>
<codeline lineno="53"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="54"><highlight class="normal">}</highlight></codeline>
<codeline lineno="55"><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal">}</highlight></codeline>
<codeline lineno="58"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_IP_MXCRYPTO)<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_trng.c" />
  </compounddef>
</doxygen>