<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__comp" kind="group">
    <compoundname>group_hal_comp</compoundname>
    <title>COMP (Analog Comparator)</title>
    <innerclass prot="public" refid="structcyhal__comp__config__t">cyhal_comp_config_t</innerclass>
    <innergroup refid="group__group__hal__results__comp">Comparator HAL Results</innergroup>
      <sectiondef kind="enum">
      <memberdef id="group__group__hal__comp_1ga9ce3883df3861eb47c5b1e3c079edc37" kind="enum" prot="public" static="no" strong="no">
        <type />
        <name>cyhal_comp_event_t</name>
        <enumvalue id="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37ae1b5c576161fce50484626bcffd1d46d" prot="public">
          <name>CYHAL_COMP_RISING_EDGE</name>
          <initializer>= 0x01</initializer>
          <briefdescription>
<para>Rising edge on comparator output. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37a77b4bfbf8689a798c2baeb7d054e0e96" prot="public">
          <name>CYHAL_COMP_FALLING_EDGE</name>
          <initializer>= 0x02</initializer>
          <briefdescription>
<para>Falling edge on comparator output. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <briefdescription>
<para><bold>cyhal_comp_event_t: </bold><linebreak />Comparator event types.</para></briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="108" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" bodystart="104" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="105" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="typedef">
      <memberdef id="group__group__hal__comp_1gac0cf713e6a1b5bca220e9da2f60a8d9e" kind="typedef" prot="public" static="no">
        <type>void(*</type>
        <definition>typedef void(* cyhal_comp_event_callback_t) (void *callback_arg, cyhal_comp_event_t event)</definition>
        <argsstring>)(void *callback_arg, cyhal_comp_event_t event)</argsstring>
        <name>cyhal_comp_event_callback_t</name>
        <briefdescription>
<para>Handler for Comparator events. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Not all hardware is capable of differentiating which type of edge triggered an event when both rising and falling edges are enabled. If the edge cannot be determined, the event argument will be CYHAL_COMP_RISING_EDGE | CYHAL_COMP_FALLING_EDGE &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" bodystart="126" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="126" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga5cc60c08506afdf99bd847289b67bd7d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_comp_init</definition>
        <argsstring>(cyhal_comp_t *obj, cyhal_gpio_t vin_p, cyhal_gpio_t vin_m, cyhal_gpio_t output, cyhal_comp_config_t *cfg)</argsstring>
        <name>cyhal_comp_init</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>vin_p</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>vin_m</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>output</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__config__t">cyhal_comp_config_t</ref> *</type>
          <declname>cfg</declname>
        </param>
        <briefdescription>
<para>Initialize the Comparator peripheral. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a Comparator object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vin_p</parametername>
</parameternamelist>
<parameterdescription>
<para>Non-inverting input pin </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vin_m</parametername>
</parameternamelist>
<parameterdescription>
<para>Inverting input pin </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">output</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator output pin. May be <ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__04__80__tqfp_1gga43c4aa9a5556b7386cb604f0ab3b242ca3dbd1016ea99d087d747530418b89a01">NC</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">cfg</parametername>
</parameternamelist>
<parameterdescription>
<para>Configuration structure </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the init request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="79" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="59" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="139" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga22b207c986cfda6121fea3a00a5baf67" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_comp_free</definition>
        <argsstring>(cyhal_comp_t *obj)</argsstring>
        <name>cyhal_comp_free</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Deinitialize the Comparator peripheral. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="99" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="81" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="145" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga3808f1cb24f0dd6dc9d16152667649de" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_comp_set_power</definition>
        <argsstring>(cyhal_comp_t *obj, cyhal_power_level_t power)</argsstring>
        <name>cyhal_comp_set_power</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__general__types_1ga246fde0e79029f514ce3846b31e366d7">cyhal_power_level_t</ref></type>
          <declname>power</declname>
        </param>
        <briefdescription>
<para>Changes the current operating power level of the comparator. </para>        </briefdescription>
        <detaileddescription>
<para>If the power level is set to <ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref>, the comparator will be powered-off but it will retain its configuration, so it is not necessary to reconfigure it when changing the power level from <ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref> to any other value.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">power</parametername>
</parameternamelist>
<parameterdescription>
<para>The power level to set </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the set power request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="120" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="101" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="157" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga05458b0c95ab66c699787f5fe118fa99" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_comp_configure</definition>
        <argsstring>(cyhal_comp_t *obj, cyhal_comp_config_t *cfg)</argsstring>
        <name>cyhal_comp_configure</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__config__t">cyhal_comp_config_t</ref> *</type>
          <declname>cfg</declname>
        </param>
        <briefdescription>
<para>Reconfigure the Comparator. </para>        </briefdescription>
        <detaileddescription>
<para>This retains the powered state of the comparator. Depending on the implementation, it may be necessary to temporarily deconfigure and/or power off the comparator in order to apply the new configuration. However, if the comparator is powered-off when this function is called, it will remain powered-off after it returns. Likewise, if the comparator is powered-on when this function is called, it will remain powered-on after it returns.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">cfg</parametername>
</parameternamelist>
<parameterdescription>
<para>New configuration to apply </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the configure request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="141" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="122" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="172" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga6b4cece78ae5cd76db2fb93493914e8d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>bool</type>
        <definition>bool cyhal_comp_read</definition>
        <argsstring>(cyhal_comp_t *obj)</argsstring>
        <name>cyhal_comp_read</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Reads the Comparator state. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The Comparator state. True if the non-inverting pin voltage is greater than the inverting pin voltage, false otherwise. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="161" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="143" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="180" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga0baa90d0b7b567226d997fbc09e07843" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_comp_register_callback</definition>
        <argsstring>(cyhal_comp_t *obj, cyhal_comp_event_callback_t callback, void *callback_arg)</argsstring>
        <name>cyhal_comp_register_callback</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__comp_1gac0cf713e6a1b5bca220e9da2f60a8d9e">cyhal_comp_event_callback_t</ref></type>
          <declname>callback</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>callback_arg</declname>
        </param>
        <briefdescription>
<para>Register/clear a callback handler for Comparator events. </para>        </briefdescription>
        <detaileddescription>
<para>This function will be called when one of the events enabled by <ref kindref="member" refid="group__group__hal__comp_1ga101a186f1f62ae222e9a7fae8256ee63">cyhal_comp_enable_event</ref> occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">callback</parametername>
</parameternamelist>
<parameterdescription>
<para>Function to call when the specified event happens </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">callback_arg</parametername>
</parameternamelist>
<parameterdescription>
<para>Generic argument that will be provided to the handler when called </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="172" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="163" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="190" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__comp_1ga101a186f1f62ae222e9a7fae8256ee63" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_comp_enable_event</definition>
        <argsstring>(cyhal_comp_t *obj, cyhal_comp_event_t event, uint8_t intr_priority, bool enable)</argsstring>
        <name>cyhal_comp_enable_event</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__comp_1ga9ce3883df3861eb47c5b1e3c079edc37">cyhal_comp_event_t</ref></type>
          <declname>event</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>intr_priority</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>enable</declname>
        </param>
        <briefdescription>
<para>Enable or Disable a Comparator event. </para>        </briefdescription>
        <detaileddescription>
<para>When an enabled event occurs, the function specified by <ref kindref="member" refid="group__group__hal__comp_1ga0baa90d0b7b567226d997fbc09e07843">cyhal_comp_register_callback</ref> will be called.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">event</parametername>
</parameternamelist>
<parameterdescription>
<para>Comparator event </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">intr_priority</parametername>
</parameternamelist>
<parameterdescription>
<para>Priority for NVIC interrupt events </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">enable</parametername>
</parameternamelist>
<parameterdescription>
<para>True to turn on event, False to turn off </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="194" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_comp.c" bodystart="174" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_comp.h" line="201" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>High level interface for interacting with an analog Comparator. </para>    </briefdescription>
    <detaileddescription>

<para><heading level="1">Features</heading></para>
<para>The analog comparator measures one input voltage from the non-inverting pin against a second voltage provided on the inverting pin. The result of this comparison can be used in three ways:<itemizedlist>
<listitem><para>Output to pin</para></listitem><listitem><para>Read state via firmware (see <ref kindref="member" refid="group__group__hal__comp_1ga6b4cece78ae5cd76db2fb93493914e8d">cyhal_comp_read</ref>)</para></listitem><listitem><para>Event triggered on rising or falling edge (see <ref kindref="member" refid="group__group__hal__comp_1ga9ce3883df3861eb47c5b1e3c079edc37">cyhal_comp_event_t</ref>)</para></listitem></itemizedlist>
</para><para>These three abilities can be used in any combination.</para>

<para><heading level="1">Quickstart</heading></para>
<para>Call <ref kindref="member" refid="group__group__hal__comp_1ga5cc60c08506afdf99bd847289b67bd7d">cyhal_comp_init</ref> to initialize a comparator instance by providing the comparator object (<bold>obj</bold>), non-inverting input pin (<bold>vin_p</bold>), inverting input pin (<bold>vin_m</bold>), optional output pin (<bold>output</bold>), and configuration (<bold>cfg</bold>).</para><para>Use <ref kindref="member" refid="group__group__hal__comp_1ga6b4cece78ae5cd76db2fb93493914e8d">cyhal_comp_read</ref> to read the comparator state from firmware.</para><para>Use <ref kindref="member" refid="group__group__hal__comp_1ga0baa90d0b7b567226d997fbc09e07843">cyhal_comp_register_callback</ref> and <ref kindref="member" refid="group__group__hal__comp_1ga101a186f1f62ae222e9a7fae8256ee63">cyhal_comp_enable_event</ref> to configure a callback that will be invoked on a rising and/or falling edge of the comparator output.</para>

<para><heading level="1">Code Snippets:</heading></para>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Error checking is omitted for clarity &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>

<para><heading level="1">Snippet 1: Comparator initialization</heading></para>
<para>The following snippet initializes the comparator and powers it on <programlisting filename="comp.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref><sp />comp_obj;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__comp__config__t">cyhal_comp_config_t</ref><sp />config<sp />=<sp />{<sp />.<ref kindref="member" refid="structcyhal__comp__config__t_1aa0a64636f172477d20e66a6b735f5145">power</ref><sp />=<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a4d8ce140c28177e31de20cf8da8e2a99">CYHAL_POWER_LEVEL_HIGH</ref>,<sp />.hysteresis<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal"><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />comparator,<sp />using<sp />pin<sp />P9_0<sp />for<sp />the<sp />input,<sp />pin<sp />P9_1<sp />for<sp />the<sp />reference<sp />and<sp />P9_2<sp />for<sp />the<sp />output<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga5cc60c08506afdf99bd847289b67bd7d">cyhal_comp_init</ref>(&amp;comp_obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51ac556dc6692d40142383b444a0545fe6d">P9_0</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a95e8336ec074bbd5096a3c9e2195a514">P9_1</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a39b31257ba13f758407bb87aa9d02526">P9_2</ref>,<sp />&amp;config);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Release<sp />comparator<sp />object<sp />after<sp />use<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__comp_1ga22b207c986cfda6121fea3a00a5baf67">cyhal_comp_free</ref>(&amp;comp_obj);</highlight></codeline>
</programlisting> </para>

<para><heading level="1">Snippet 2: Comparator read value</heading></para>
<para>The following snippet reads the current comparator value into a variable <programlisting filename="comp.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref><sp />comp_obj;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__comp__config__t">cyhal_comp_config_t</ref><sp />config<sp />=<sp />{<sp />.<ref kindref="member" refid="structcyhal__comp__config__t_1aa0a64636f172477d20e66a6b735f5145">power</ref><sp />=<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a4d8ce140c28177e31de20cf8da8e2a99">CYHAL_POWER_LEVEL_HIGH</ref>,<sp />.hysteresis<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal"><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />comparator,<sp />using<sp />pin<sp />P9_0<sp />for<sp />the<sp />input<sp />and<sp />pin<sp />P9_1<sp />for<sp />the<sp />reference.<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />No<sp />output<sp />pin<sp />is<sp />used.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga5cc60c08506afdf99bd847289b67bd7d">cyhal_comp_init</ref>(&amp;comp_obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51ac556dc6692d40142383b444a0545fe6d">P9_0</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a95e8336ec074bbd5096a3c9e2195a514">P9_1</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a3dbd1016ea99d087d747530418b89a01">NC</ref>,<sp />&amp;config);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Read<sp />the<sp />comparator<sp />value<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />comp_value<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga6b4cece78ae5cd76db2fb93493914e8d">cyhal_comp_read</ref>(&amp;comp_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Release<sp />comparator<sp />object<sp />after<sp />use<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__comp_1ga22b207c986cfda6121fea3a00a5baf67">cyhal_comp_free</ref>(&amp;comp_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting> </para>

<para><heading level="1">Snippet 3: Comparator event registration</heading></para>
<para>The following snippet registers a callback that will be called on either a rising or falling edge of the comparator output. <programlisting filename="comp.c"><codeline><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />comp_event_handler(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">*<sp />arg,<sp /><ref kindref="member" refid="group__group__hal__comp_1ga9ce3883df3861eb47c5b1e3c079edc37">cyhal_comp_event_t</ref><sp />event)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_UNUSED_PARAMETER(arg);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Note:<sp />arg<sp />is<sp />configured<sp />below<sp />to<sp />be<sp />a<sp />pointer<sp />to<sp />the<sp />cyhal_comp_t<sp />instance<sp />that<sp />caused<sp />the<sp />event<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(0u<sp />!=<sp />(event<sp />&amp;<sp /><ref kindref="member" refid="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37ae1b5c576161fce50484626bcffd1d46d">CYHAL_COMP_RISING_EDGE</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Handle<sp />rising<sp />edge<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(0u<sp />!=<sp />(event<sp />&amp;<sp /><ref kindref="member" refid="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37a77b4bfbf8689a798c2baeb7d054e0e96">CYHAL_COMP_FALLING_EDGE</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Handle<sp />falling<sp />edge<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />snippet_cyhal_comp_event_handling(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__comp__t">cyhal_comp_t</ref><sp />comp_obj;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />object<sp />as<sp />shown<sp />in<sp />Snippet<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />a<sp />callback<sp />and<sp />set<sp />the<sp />callback<sp />argument<sp />to<sp />be<sp />a<sp />pointer<sp />to<sp />the<sp />comparator<sp />object,</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />so<sp />that<sp />we<sp />can<sp />easily<sp />reference<sp />it<sp />from<sp />the<sp />callback<sp />handler.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__comp_1ga0baa90d0b7b567226d997fbc09e07843">cyhal_comp_register_callback</ref>(&amp;comp_obj,<sp />&amp;comp_event_handler,<sp />&amp;comp_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Subscribe<sp />to<sp />both<sp />the<sp />rising<sp />and<sp />falling<sp />edges,<sp />so<sp />that<sp />the<sp />event<sp />handler<sp />is<sp />called<sp />whenever</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />the<sp />comparator<sp />output<sp />changes</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__comp_1ga101a186f1f62ae222e9a7fae8256ee63">cyhal_comp_enable_event</ref>(&amp;comp_obj,<sp />(<ref kindref="member" refid="group__group__hal__comp_1ga9ce3883df3861eb47c5b1e3c079edc37">cyhal_comp_event_t</ref>)(<ref kindref="member" refid="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37ae1b5c576161fce50484626bcffd1d46d">CYHAL_COMP_RISING_EDGE</ref><sp />|<sp /><ref kindref="member" refid="group__group__hal__comp_1gga9ce3883df3861eb47c5b1e3c079edc37a77b4bfbf8689a798c2baeb7d054e0e96">CYHAL_COMP_FALLING_EDGE</ref>),<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gad822a466fc63847114add720ef84c83a">CYHAL_ISR_PRIORITY_DEFAULT</ref>,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting> </para>

<para><heading level="1">Snippet 4: Comparator powering-off and on</heading></para>
<para>The following snippet demonstrates temporarily powering-off the comparator without freeing it. <programlisting filename="comp.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />This<sp />assumes<sp />that<sp />the<sp />comparator<sp />has<sp />already<sp />been<sp />initialized<sp />as<sp />shown<sp />in<sp />snippet<sp />1<sp />or<sp />2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Power-on<sp />the<sp />comparator<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga3808f1cb24f0dd6dc9d16152667649de">cyhal_comp_set_power</ref>(&amp;comp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga3808f1cb24f0dd6dc9d16152667649de">cyhal_comp_set_power</ref>(&amp;comp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />When<sp />the<sp />comparator<sp />is<sp />needed<sp />again,<sp />power<sp />it<sp />back<sp />on<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__comp_1ga3808f1cb24f0dd6dc9d16152667649de">cyhal_comp_set_power</ref>(&amp;comp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
</programlisting></para>
    </detaileddescription>
  </compounddef>
</doxygen>