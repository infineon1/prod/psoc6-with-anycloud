<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__wdt" kind="group">
    <compoundname>group_hal_wdt</compoundname>
    <title>WDT (Watchdog Timer)</title>
    <innergroup refid="group__group__hal__results__wdt">WDT HAL Results</innergroup>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1ga4f2a8a684b8a76b92b9417637bb55697" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_wdt_init</definition>
        <argsstring>(cyhal_wdt_t *obj, uint32_t timeout_ms)</argsstring>
        <name>cyhal_wdt_init</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeout_ms</declname>
        </param>
        <briefdescription>
<para>Initialize and start the WDT. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The specified timeout must be at least 1ms and at most the WDT's maximum timeout (see &lt;a href="group__group__hal__wdt.html#group__group__hal__wdt_1gaf121c3f36d38b275c1887b8fddf96a3f"&gt;cyhal_wdt_get_max_timeout_ms()&lt;/a&gt;).&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a WDT object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="inout">timeout_ms</parametername>
</parameternamelist>
<parameterdescription>
<para>The time in milliseconds before the WDT times out (1ms - max) (see <ref kindref="member" refid="group__group__hal__wdt_1gaf121c3f36d38b275c1887b8fddf96a3f">cyhal_wdt_get_max_timeout_ms()</ref>) </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the init request</para></simplesect>
Returns <ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref> if the operation was successfull. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="115" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="80" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="95" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1ga976a8b0fe8fe66f89311cfca18768b13" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_wdt_free</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_free</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Free the WDT. </para>        </briefdescription>
        <detaileddescription>
<para>Powers down the WDT. Releases object (obj). After calling this function no other WDT functions should be called except <ref kindref="member" refid="group__group__hal__wdt_1ga4f2a8a684b8a76b92b9417637bb55697">cyhal_wdt_init()</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="123" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="117" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="107" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1gaeb43bee6a246da3eedfb5d06fbd226de" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_wdt_kick</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_kick</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Resets the WDT. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called periodically to prevent the WDT from timing out and resetting the device.</para><para>See <ref kindref="member" refid="group__group__hal__wdt_1subsection_wdt_snippet1">Snippet 1: Initialize the WDT and kick periodically</ref></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="135" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="125" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="118" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1gaae8da9770e8e061c341bf9f604b91260" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_wdt_start</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_start</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Start (enable) the WDT. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the start request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="143" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="137" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="125" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1gac4a8ed58907508cc2ebf9a29c2d86f39" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_wdt_stop</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_stop</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Stop (disable) the WDT. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="inout">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the stop request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="150" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="145" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="132" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1ga84de4fa0ac7dbacb681e82a5df94f6d5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t cyhal_wdt_get_timeout_ms</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_get_timeout_ms</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Get the WDT timeout. </para>        </briefdescription>
        <detaileddescription>
<para>Gets the configured time, in milliseconds, before the WDT times out.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The time in milliseconds before the WDT times out </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="156" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="152" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="141" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1gaf121c3f36d38b275c1887b8fddf96a3f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t cyhal_wdt_get_max_timeout_ms</definition>
        <argsstring>(void)</argsstring>
        <name>cyhal_wdt_get_max_timeout_ms</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Gets the maximum WDT timeout in milliseconds. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The maximum timeout for the WDT </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="161" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="158" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="147" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__wdt_1gae57e067322093d0170563c9609890f7c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>bool</type>
        <definition>bool cyhal_wdt_is_enabled</definition>
        <argsstring>(cyhal_wdt_t *obj)</argsstring>
        <name>cyhal_wdt_is_enabled</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Check if WDT is enabled. </para>        </briefdescription>
        <detaileddescription>
<para>This will return true after <ref kindref="member" refid="group__group__hal__wdt_1gaae8da9770e8e061c341bf9f604b91260">cyhal_wdt_start</ref> is called. It will return false before the WDT is started, or after <ref kindref="member" refid="group__group__hal__wdt_1gac4a8ed58907508cc2ebf9a29c2d86f39">cyhal_wdt_stop</ref> is called.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The WDT object </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of WDT is_enabled request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="167" bodyfile="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_wdt.c" bodystart="163" column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_wdt.h" line="155" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para><linebreak />
</para><para>High level interface to the Watchdog Timer (WDT). </para>    </briefdescription>
    <detaileddescription>
<para>The WDT can be used for recovering from a CPU or firmware failure. The WDT is initialized with a timeout interval. Once the WDT is started, <ref kindref="member" refid="group__group__hal__wdt_1gaeb43bee6a246da3eedfb5d06fbd226de">cyhal_wdt_kick</ref> must be called at least once within each timeout interval to reset the count. In case the firmware fails to do so, it is considered to be a CPU crash or firmware failure and the device will be reset.</para>
<para><heading level="1">Features</heading></para>
<para>WDT resets the device if the WDT is not "kicked" using <ref kindref="member" refid="group__group__hal__wdt_1gaeb43bee6a246da3eedfb5d06fbd226de">cyhal_wdt_kick</ref> within the configured timeout interval.</para>

<para><heading level="1">Quick Start</heading></para>
<para><ref kindref="member" refid="group__group__hal__wdt_1ga4f2a8a684b8a76b92b9417637bb55697">cyhal_wdt_init()</ref> is used to initialize the WDT by providing the WDT object (<bold>obj</bold>) and the timeout (<bold>timeout_ms</bold>) value in milliseconds. The timeout parameter can have a minimum value of 1ms. The maximum value of the timeout parameter can be obtained using the <ref kindref="member" refid="group__group__hal__wdt_1gaf121c3f36d38b275c1887b8fddf96a3f">cyhal_wdt_get_max_timeout_ms()</ref>.</para>

<para><heading level="1">Code Snippet</heading></para>

<para><bold>Snippet 1: Initialize the WDT and kick periodically</bold></para>
<para>The following snippet initializes the WDT and illustrates how to reset the WDT within the timeout interval. <programlisting filename="wdt.c"><codeline><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />LED_ON<sp /><sp /><sp /><sp /><sp /><sp />0u</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />LED_OFF<sp /><sp /><sp /><sp /><sp />1u</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__wdt__t">cyhal_wdt_t</ref><sp />wdt_obj;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />timeout_ms<sp />=<sp />2000;<sp /></highlight><highlight class="comment">/*<sp />Minimum<sp />value<sp />of<sp />timeout_ms<sp />is<sp />1ms<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initializing<sp />WDT<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__wdt_1ga4f2a8a684b8a76b92b9417637bb55697">cyhal_wdt_init</ref>(&amp;wdt_obj,<sp />timeout_ms);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(<ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref><sp />==<sp />rslt);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />pin<sp />connected<sp />to<sp />an<sp />LED<sp />on<sp />the<sp />board<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__gpio_1gab93322030909d3af6a9fc1a3b2eccbaa">cyhal_gpio_init</ref>(CYBSP_USER_LED,<sp /><ref kindref="member" refid="group__group__hal__gpio_1gga54f023b4b455556095b64f4f5d7e3446a846ce42fac8d332495aaa45b93d671ec">CYHAL_GPIO_DIR_OUTPUT</ref>,<sp /><ref kindref="member" refid="group__group__hal__gpio_1gga2eb76143375648febb0ef73f00a681b2a697e0520cb1b71b748bb76eaafb3ad1b">CYHAL_GPIO_DRIVE_STRONG</ref>,<sp />CYBSP_LED_STATE_OFF);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Device<sp />power-on<sp />reset<sp />or<sp />XRES<sp />event<sp />-<sp />blink<sp />LED<sp />once<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__gpio_1gaf66e7c3ed7b3b0711635d7687ae92291">cyhal_gpio_write</ref>(CYBSP_USER_LED,<sp />CYBSP_LED_STATE_ON);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__system_1ga5f450769c1207d98134a9ced39adfdda">cyhal_system_delay_ms</ref>(100);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__gpio_1gaf66e7c3ed7b3b0711635d7687ae92291">cyhal_gpio_write</ref>(CYBSP_USER_LED,<sp />CYBSP_LED_STATE_OFF);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__system_1ga5f450769c1207d98134a9ced39adfdda">cyhal_system_delay_ms</ref>(100);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Device<sp />has<sp />restarted<sp />due<sp />to<sp />Watchdog<sp />timeout<sp />-<sp />blink<sp />a<sp />second<sp />time<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__hal__system_1gga53ba6a7416574a1b03890d23b7a90264a28d2eb1bf90f731f930c487d8dcd8079">CYHAL_SYSTEM_RESET_WDT</ref><sp />==<sp /><ref kindref="member" refid="group__group__hal__system_1ga9a7b601e0b06a896860f411103a4abc6">cyhal_system_get_reset_reason</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__gpio_1gaf66e7c3ed7b3b0711635d7687ae92291">cyhal_gpio_write</ref>(CYBSP_USER_LED,<sp />CYBSP_LED_STATE_ON);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__system_1ga5f450769c1207d98134a9ced39adfdda">cyhal_system_delay_ms</ref>(100);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__gpio_1gaf66e7c3ed7b3b0711635d7687ae92291">cyhal_gpio_write</ref>(CYBSP_USER_LED,<sp />CYBSP_LED_STATE_OFF);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__system_1ga5f450769c1207d98134a9ced39adfdda">cyhal_system_delay_ms</ref>(100);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />WDT<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__wdt_1ga4f2a8a684b8a76b92b9417637bb55697">cyhal_wdt_init</ref>(&amp;wdt_obj,<sp />timeout_ms);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(<sp />;<sp />;<sp />)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />This<sp />delay<sp />emulates<sp />an<sp />application<sp />task<sp />executed<sp />between<sp />subsequent</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />WDT<sp />kicks.<sp />Increasing<sp />this<sp />delay<sp />to<sp />beyond<sp />the<sp />programmed<sp />WDT<sp />timeout<sp />will</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cause<sp />a<sp />device<sp />reset.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__system_1ga5f450769c1207d98134a9ced39adfdda">cyhal_system_delay_ms</ref>(1000);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Reset<sp />the<sp />WDT<sp />and<sp />avoid<sp />a<sp />device<sp />reset<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__wdt_1gaeb43bee6a246da3eedfb5d06fbd226de">cyhal_wdt_kick</ref>(&amp;wdt_obj);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>

    </detaileddescription>
  </compounddef>
</doxygen>