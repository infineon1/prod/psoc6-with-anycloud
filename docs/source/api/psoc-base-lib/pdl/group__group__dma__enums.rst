=================
Enumerated Types
=================


.. doxygengroup:: group_dma_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: