===============
Interrupt Masks
===============

.. doxygengroup:: group_i2s_macros_interrupt_masks
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
