==================
Enumerated Types
==================

.. doxygengroup:: group_prot_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: