================
Data Structures
================

.. doxygengroup:: group_seglcd_data_structures
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: