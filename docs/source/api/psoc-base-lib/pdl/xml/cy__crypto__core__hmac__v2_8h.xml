<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cy__crypto__core__hmac__v2_8h" kind="file" language="C++">
    <compoundname>cy_crypto_core_hmac_v2.h</compoundname>
    <includes local="yes" refid="cy__crypto__common_8h">cy_crypto_common.h</includes>
    <includedby local="yes" refid="cy__crypto__core__hmac_8h">cy_crypto_core_hmac.h</includedby>
    <includedby local="yes" refid="cy__crypto__core__hmac__v2_8c">cy_crypto_core_hmac_v2.c</includedby>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="cy__crypto__core__hmac__v2_8h_1ad435a2e87e6eeba7fe30fadba3063087" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Core_V2_Hmac</definition>
        <argsstring>(CRYPTO_Type *base, uint8_t *hmac, uint8_t const *message, uint32_t messageSize, uint8_t const *key, uint32_t keyLength, cy_en_crypto_sha_mode_t mode)</argsstring>
        <name>Cy_Crypto_Core_V2_Hmac</name>
        <param>
          <type>CRYPTO_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>hmac</declname>
        </param>
        <param>
          <type>uint8_t const *</type>
          <declname>message</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>messageSize</declname>
        </param>
        <param>
          <type>uint8_t const *</type>
          <declname>key</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>keyLength</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__crypto__enums_1ga66ead9efce36e261e978cad722bf2dbb">cy_en_crypto_sha_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>Performs HMAC calculation. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the CRYPTO instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>hmac</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the calculated HMAC. Must be 4-byte aligned.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>message</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a message whose hash value is being computed.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>messageSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of a message.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>key</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the key.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>keyLength</parametername>
</parameternamelist>
<parameterdescription>
<para>The length of the key.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__crypto__enums_1ga66ead9efce36e261e978cad722bf2dbb">cy_en_crypto_sha_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="335" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_core_hmac_v2.c" bodystart="302" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_hmac_v2.h" line="40" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This file provides constants and function prototypes for the API for the HMAC method in the Crypto block driver. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="version"><para>2.30.3</para></simplesect>
Copyright 2016-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para><para>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at</para><para><ulink url="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</ulink></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="27"><highlight class="preprocessor">#if<sp />!defined(CY_CRYPTO_CORE_HMAC_V2_H)</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CY_CRYPTO_CORE_HMAC_V2_H</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_crypto_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CPUSS_CRYPTO_SHA<sp />==<sp />1)</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /></codeline>
<codeline lineno="40"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Hmac(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="41"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp />*hmac,</highlight></codeline>
<codeline lineno="42"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />*message,</highlight></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />messageSize,</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />*key,</highlight></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />keyLength,</highlight></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__crypto__enums_1ga66ead9efce36e261e978cad722bf2dbb">cy_en_crypto_sha_mode_t</ref><sp />mode);</highlight></codeline>
<codeline lineno="47"><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />(CPUSS_CRYPTO_SHA<sp />==<sp />1)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="50"><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal">}</highlight></codeline>
<codeline lineno="53"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXCRYPTO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />!defined(CY_CRYPTO_CORE_HMAC_V2_H)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="58"><highlight class="normal" /></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal" /><highlight class="comment">/*<sp />[]<sp />END<sp />OF<sp />FILE<sp />*/</highlight><highlight class="normal" /></codeline>
    </programlisting>
    <location file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_hmac_v2.h" />
  </compounddef>
</doxygen>