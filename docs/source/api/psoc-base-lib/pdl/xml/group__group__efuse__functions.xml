<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__efuse__functions" kind="group">
    <compoundname>group_efuse_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga2dc80ca522388529fe573f5c46694f2f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></type>
        <definition>cy_en_efuse_status_t Cy_EFUSE_GetEfuseBit</definition>
        <argsstring>(uint32_t bitNum, bool *bitVal)</argsstring>
        <name>Cy_EFUSE_GetEfuseBit</name>
        <param>
          <type>uint32_t</type>
          <declname>bitNum</declname>
        </param>
        <param>
          <type>bool *</type>
          <declname>bitVal</declname>
        </param>
        <briefdescription>
<para>Reports the current state of a given eFuse bit-number. </para>        </briefdescription>
        <detaileddescription>
<para>Consult the device TRM to determine the target fuse bit number.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;An attempt to read an eFuse data from a protected memory region will generate a HardFault.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>bitNum</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of the bit to read. The valid range of the bit number is from 0 to EFUSE_EFUSE_NR * 32 * 8 - 1 where:<itemizedlist>
<listitem><para>EFUSE_EFUSE_NR is number of efuse macros in the selected device series,</para></listitem><listitem><para>32 is a number of fuse bytes in one efuse macro,</para></listitem><listitem><para>8 is a number of fuse bits in the byte.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
The EFUSE_EFUSE_NR macro is defined in the series-specific header file, e.g <emphasis>&lt;PDL_DIR&gt;/devices/include/psoc6_01_config</emphasis>.<emphasis>h</emphasis> </para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>bitVal</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to store the bit value.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para>The example below shows how to read device life-cycle register bits in PSoC 6: <programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />offset<sp />=<sp />offsetof(<ref kindref="compound" refid="structcy__stc__efuse__data__t">cy_stc_efuse_data_t</ref>,<sp />LIFECYCLE_STAGE.SECURE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />bitVal;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga2dc80ca522388529fe573f5c46694f2f">Cy_EFUSE_GetEfuseBit</ref>(offset,<sp />&amp;bitVal);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="100" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="79" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_efuse.h" line="176" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></type>
        <definition>cy_en_efuse_status_t Cy_EFUSE_GetEfuseByte</definition>
        <argsstring>(uint32_t offset, uint8_t *byteVal)</argsstring>
        <name>Cy_EFUSE_GetEfuseByte</name>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>byteVal</declname>
        </param>
        <briefdescription>
<para>Reports the current state of the eFuse byte. </para>        </briefdescription>
        <detaileddescription>
<para>If the offset parameter is beyond the available quantities, zeroes will be stored to the byteVal parameter. Consult the device TRM to determine the target fuse byte offset.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;An attempt to read an eFuse data from a protected memory region will generate a HardFault.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>The offset of the byte to read. The valid range of the byte offset is from 0 to EFUSE_EFUSE_NR * 32 - 1 where:<itemizedlist>
<listitem><para>EFUSE_EFUSE_NR is a number of efuse macros in the selected device series,</para></listitem><listitem><para>32 is a number of fuse bytes in one efuse macro.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
The EFUSE_EFUSE_NR macro is defined in the series-specific header file, e.g <emphasis>&lt;PDL_DIR&gt;/devices/include/psoc6_01_config</emphasis>.<emphasis>h</emphasis> </para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>byteVal</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to store eFuse data.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para>The example below shows how to read a device life-cycle stage register in PSoC 6: <programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Determine<sp />the<sp />offset<sp />of<sp />the<sp />byte<sp />to<sp />read.<sp />Divide<sp />by<sp />8<sp />since<sp />the<sp />one<sp />byte<sp />in</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp />cy_stc_efuse_data_t<sp />struct<sp />corresponds<sp />to<sp />the<sp />one<sp />bit<sp />in<sp />the<sp />eFuse<sp />memory.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />offset<sp />=<sp />offsetof(<ref kindref="compound" refid="structcy__stc__efuse__data__t">cy_stc_efuse_data_t</ref>,<sp />LIFECYCLE_STAGE.NORMAL)<sp />/<sp /><ref kindref="member" refid="group__group__efuse__macros_1ga34bda47f01021424e981b32de0d8a7f6">CY_EFUSE_BITS_PER_BYTE</ref>;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />byteVal;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6">Cy_EFUSE_GetEfuseByte</ref>(offset,<sp />&amp;byteVal);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="171" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="136" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_efuse.h" line="177" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga19b1ee83f64f5405681b37266941c3e5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_EFUSE_GetExternalStatus</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_EFUSE_GetExternalStatus</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function handles the case where a module such as a security image captures a system call from this driver and reports its own status or error code, for example, protection violation. </para>        </briefdescription>
        <detaileddescription>
<para>In that case, a function from this driver returns an unknown error (see <ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref>). After receipt of an unknown error, the user may call this function to get the status of the capturing module.</para><para>The user is responsible for parsing the content of the returned value and casting it to the appropriate enumeration.</para><para><simplesect kind="return"><para>The error code of the previous efuse operation. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="195" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="192" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_efuse.h" line="178" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>