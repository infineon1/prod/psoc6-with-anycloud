<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__clk__peri__funcs" kind="group">
    <compoundname>group_sysclk_clk_peri_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_ClkPeriSetDivider</definition>
        <argsstring>(uint8_t divider)</argsstring>
        <name>Cy_SysClk_ClkPeriSetDivider</name>
        <param>
          <type>uint8_t</type>
          <declname>divider</declname>
        </param>
        <briefdescription>
<para>Sets the clock divider for the peripheral clock tree. </para>        </briefdescription>
        <detaileddescription>
<para>All peripheral clock dividers are sourced from this clock. Also the Cortex M0+ clock divider is sourced from this clock. The source of this divider is clkHf[0]</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>divider</parametername>
</parameternamelist>
<parameterdescription>
<para>divider value between 0 and 255 Causes integer division of (divider value + 1), or division by 1 to 256.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call &lt;a href="group__group__system__config__system__functions.html#group__group__system__config__system__functions_1gae0c36a9591fe6e9c45ecb21a794f0f0f"&gt;SystemCoreClockUpdate&lt;/a&gt; after this function calling.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Peri<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />majority<sp />of<sp />peripherals<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/2</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(1U<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga52444654d4f896de2a75e6db71705afd">Cy_SysClk_ClkPeriGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1U);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />Peri<sp />clock<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPerifreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga6ba76776507e1f3cadcf693c19710f44">Cy_SysClk_ClkPeriGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2536" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="2529" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="2484" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__peri__funcs_1ga52444654d4f896de2a75e6db71705afd" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint8_t</type>
        <definition>__STATIC_INLINE uint8_t Cy_SysClk_ClkPeriGetDivider</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_ClkPeriGetDivider</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns the clock divider of the peripheral (peri) clock. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The divider value. The integer division done is by (divider value + 1), or division by 1 to 256.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Peri<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />majority<sp />of<sp />peripherals<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/2</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(1U<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga52444654d4f896de2a75e6db71705afd">Cy_SysClk_ClkPeriGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1U);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />Peri<sp />clock<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPerifreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga6ba76776507e1f3cadcf693c19710f44">Cy_SysClk_ClkPeriGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2555" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="2552" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="2485" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__clk__peri__funcs_1ga6ba76776507e1f3cadcf693c19710f44" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SysClk_ClkPeriGetFrequency</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_ClkPeriGetFrequency</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the frequency of the peri clock. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The frequency, in Hz.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />HFCLK0<sp />is<sp />configured<sp />and<sp />enabled.<sp />The<sp />Peri<sp />clock<sp />sourcing<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />majority<sp />of<sp />peripherals<sp />must<sp />run<sp />at<sp />a<sp />frequency<sp />that<sp />is<sp />1/2</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />of<sp />HFCLK0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(1U<sp />!=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga52444654d4f896de2a75e6db71705afd">Cy_SysClk_ClkPeriGetDivider</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga43eae2e50299c3fe43d3fa9f0442af20">Cy_SysClk_ClkPeriSetDivider</ref>(1U);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />Peri<sp />clock<sp />output<sp />frequency<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />clkPerifreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peri__funcs_1ga6ba76776507e1f3cadcf693c19710f44">Cy_SysClk_ClkPeriGetFrequency</ref>();</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2508" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="2501" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="2486" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>