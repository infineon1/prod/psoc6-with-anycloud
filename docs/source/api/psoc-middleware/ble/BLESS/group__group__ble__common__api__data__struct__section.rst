=================
Data Structures
=================

.. doxygengroup:: group_ble_common_api_data_struct_section
   :project: bless

.. toctree::
   :hidden:

   group__group__ble__common__api__definitions.rst
   group__group__ble__common__api__gap__definitions.rst
   group__group__ble__common__api__gatt__definitions.rst
   group__group__ble__common__api__l2cap__definitions.rst