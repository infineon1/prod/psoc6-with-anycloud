=======================
HID Service (HIDS)
=======================

.. doxygengroup:: group_ble_service_api_HIDS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___h_i_d_s__server__client.rst
   group__group__ble__service__api___h_i_d_s__server.rst
   group__group__ble__service__api___h_i_d_s__client.rst
   group__group__ble__service__api___h_i_d_s__definitions.rst