=========================
Heart Rate Service (HRS)
=========================

.. doxygengroup:: group_ble_service_api_HRS
   :project: bless
   
   
.. toctree::
   :hidden:

   group__group__ble__service__api___h_r_s__server__client.rst
   group__group__ble__service__api___h_r_s__server.rst
   group__group__ble__service__api___h_r_s__client.rst
   group__group__ble__service__api___h_r_s__definitions.rsts