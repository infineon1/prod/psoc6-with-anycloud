======================================
Reference Time Update Service (RTUS)
======================================

.. doxygengroup:: group_ble_service_api_RTUS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___r_t_u_s__server__client.rst
   group__group__ble__service__api___r_t_u_s__server.rst
   group__group__ble__service__api___r_t_u_s__client.rst
   group__group__ble__service__api___r_t_u_s__definitions.rst