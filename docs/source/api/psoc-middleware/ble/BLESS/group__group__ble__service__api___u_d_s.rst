=========================
User Data Service (UDS)
=========================

.. doxygengroup:: group_ble_service_api_UDS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___u_d_s__server__client.rst
   group__group__ble__service__api___u_d_s__server.rst
   group__group__ble__service__api___u_d_s__client.rst
   group__group__ble__service__api___u_d_s__definitions.rst