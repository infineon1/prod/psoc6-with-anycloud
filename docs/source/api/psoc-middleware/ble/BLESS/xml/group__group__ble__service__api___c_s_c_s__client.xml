<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___c_s_c_s__client" kind="group">
    <compoundname>group_ble_service_api_CSCS_client</compoundname>
    <title>CSCS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_s_c_s__client_1ga5e6bc608170a34e2d7696b91a119debc" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CSCSC_SetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cscs_char_index_t charIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CSCSC_SetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1ga8e463f0d89f302da57ac1befaf4143bc">cy_en_ble_cscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>This function is used to write the characteristic (which is identified by charIndex) value attribute in the server. </para>        </briefdescription>
        <detaileddescription>
<para>As a result a Write Request is sent to the GATT Server and on successful execution of the request on the server side, the <ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a9aa2731c2c653e97d133d719b4ac7743">CY_BLE_EVT_CSCSS_WRITE_CHAR</ref> event is generated. On successful request execution on the server side, the Write Response is sent to the client.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of the characteristic value attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the characteristic value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
If the CSCS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__server__client_1gac53c280acaaf43852c73e1ec8f4f9601">Cy_BLE_CSCS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a37cbae871bfd3dc7fff264e5824810de">CY_BLE_EVT_CSCSC_WRITE_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cscs__char__value__t">cy_stc_ble_cscs_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the CSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - In case if the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1291" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.c" bodystart="1242" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.h" line="195" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_s_c_s__client_1ga735bb87e59d42113b85f0926bf61bf4e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CSCSC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cscs_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_CSCSC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1ga8e463f0d89f302da57ac1befaf4143bc">cy_en_ble_cscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to peer device to get characteristic value of the Cycling Speed and Cadence service, which is identified by charIndex. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Peer device doesn't have a particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CSCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__server__client_1gac53c280acaaf43852c73e1ec8f4f9601">Cy_BLE_CSCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51aa18988559d6fea152ea268ebf37f44e4">CY_BLE_EVT_CSCSC_READ_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex , value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cscs__char__value__t">cy_stc_ble_cscs_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise, if the CSCS service-specific callback is not registered:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - In case if the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1395" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.c" bodystart="1334" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.h" line="199" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_s_c_s__client_1gaedbb17a3bd7ffb3c23674c21cac7948d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CSCSC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cscs_char_index_t charIndex, cy_en_ble_cscs_descr_index_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CSCSC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1ga8e463f0d89f302da57ac1befaf4143bc">cy_en_ble_cscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1gac3c2057d18209c919dd3001b8df48ab5">cy_en_ble_cscs_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>Sends a request to peer device to get characteristic descriptor of specified characteristic of the Cycling Speed and Cadence service. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, Write Request is sent to the GATT Server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a13281a9221e02b334dcbb1ab9d273e7a">CY_BLE_EVT_CSCSS_NOTIFICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ad98937a04c8a6128ba842c1da50ad19d">CY_BLE_EVT_CSCSS_NOTIFICATION_DISABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a697321d958b322ba0fd6c272ed7088a8">CY_BLE_EVT_CSCSS_INDICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a4cbe3c8d7b220e31686e09020cc8436b">CY_BLE_EVT_CSCSS_INDICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a CSCS characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a CSCS characteristic descriptor. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
If the CSCS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__server__client_1gac53c280acaaf43852c73e1ec8f4f9601">Cy_BLE_CSCS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a662f88b75478654363c7ea6158c65910">CY_BLE_EVT_CSCSC_WRITE_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, descrIndex etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cscs__descr__value__t">cy_stc_ble_cscs_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the CSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - In case if the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1512" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.c" bodystart="1449" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.h" line="202" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_s_c_s__client_1ga69e0b53b3be5ce3987760b948f8b281e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CSCSC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cscs_char_index_t charIndex, cy_en_ble_cscs_descr_index_t descrIndex)</argsstring>
        <name>Cy_BLE_CSCSC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1ga8e463f0d89f302da57ac1befaf4143bc">cy_en_ble_cscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__definitions_1gac3c2057d18209c919dd3001b8df48ab5">cy_en_ble_cscs_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to peer device to get characteristic descriptor of specified characteristic of the Cycling Speed and Cadence service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service Characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic descriptor.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;This operation is not permitted on the specified attribute. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CSCS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_s_c_s__server__client_1gac53c280acaaf43852c73e1ec8f4f9601">Cy_BLE_CSCS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a2cd95777ceb79af9c948d5fd6f4f0160">CY_BLE_EVT_CSCSC_READ_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cscs__descr__value__t">cy_stc_ble_cscs_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the CSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - In case if the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>).</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1609" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.c" bodystart="1555" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.h" line="207" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_s_c_s__client_1gac440ae6d958145e650f788e8cc3be33f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CSCSC_StoreProfileData</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_CSCSC_StoreProfileData</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Stores the structure with discovered attributes of Cycling Speed and Cadence Server device to the flash. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL as cy_ble_cscscConfigPtr. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_FLASH_WRITE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;An error in the flash Write. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1643" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.c" bodystart="1629" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cscs.h" line="210" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to CSCS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_CSCSC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>