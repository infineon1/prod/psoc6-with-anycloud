<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___r_s_c_s__client" kind="group">
    <compoundname>group_ble_service_api_RSCS_client</compoundname>
    <title>RSCS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___r_s_c_s__client_1ga833859e3eee07cb626734a9cf4eee49a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RSCSC_SetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_rscs_char_index_t charIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_RSCSC_SetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__definitions_1ga24f372ada267f7627ed72ce0640890a5">cy_en_ble_rscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>This function is used to write the characteristic (which is identified by charIndex) value attribute in the server. </para>        </briefdescription>
        <detaileddescription>
<para>As a result a Write Request is sent to the GATT Server and on successful execution of the request on the server side, the <ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a10339e9519366c6a2776056f62acf65c">CY_BLE_EVT_RSCSS_WRITE_CHAR</ref> event is generated. On successful request execution on the server side, the Write Response is sent to the client.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of the characteristic value attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the characteristic value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the RSCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__server__client_1ga9139a40b49e73673f7a2d1b72cefa027">Cy_BLE_RSCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ae1aff6bde55997717706d201734f9c23">CY_BLE_EVT_RSCSC_WRITE_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__rscs__char__value__t">cy_stc_ble_rscs_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the RSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - In case if the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1282" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.c" bodystart="1234" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.h" line="193" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___r_s_c_s__client_1ga84ef776fb33e1c4cd222196a47c8c385" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RSCSC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_rscs_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_RSCSC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__definitions_1ga24f372ada267f7627ed72ce0640890a5">cy_en_ble_rscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to the peer device to set the characteristic value of the Running Speed and Cadence service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The Read Request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the RSCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__server__client_1ga9139a40b49e73673f7a2d1b72cefa027">Cy_BLE_RSCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a9e09b30924188f6d86a568f775a47aa2">CY_BLE_EVT_RSCSC_READ_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex , value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__rscs__char__value__t">cy_stc_ble_rscs_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the RSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - In case if the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>).</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1386" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.c" bodystart="1330" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.h" line="197" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___r_s_c_s__client_1gad2ab1b1ecda89832e821925d03949ada" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RSCSC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_rscs_char_index_t charIndex, cy_en_ble_rscs_descr_index_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_RSCSC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__definitions_1ga24f372ada267f7627ed72ce0640890a5">cy_en_ble_rscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__definitions_1gabaf51d95af1aa0f1613e6718e59e9ac9">cy_en_ble_rscs_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>Sends a request to the peer device to get the characteristic descriptor of the specified characteristic of the Running Speed and Cadence service. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, Write Request is sent to the GATT Server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a882e0427699c87579c0456731a01d24e">CY_BLE_EVT_RSCSS_NOTIFICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a2141b345b15c322cc50a7f7d17799baa">CY_BLE_EVT_RSCSS_NOTIFICATION_DISABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a9935553a13a175d673c81665ee666da8">CY_BLE_EVT_RSCSS_INDICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a5e514c607443d169af7b828c57fd483d">CY_BLE_EVT_RSCSS_INDICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a RSCS characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a RSCS characteristic descriptor. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the RSCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__server__client_1ga9139a40b49e73673f7a2d1b72cefa027">Cy_BLE_RSCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a4bfdf984b070ee0325bff5245ca51453">CY_BLE_EVT_RSCSC_WRITE_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, descrIndex etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__rscs__descr__value__t">cy_stc_ble_rscs_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the RSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - In case if the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1506" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.c" bodystart="1443" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.h" line="200" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___r_s_c_s__client_1ga653f2f4852753777fcfc4dc70cc9f7ad" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_RSCSC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_rscs_char_index_t charIndex, uint8_t descrIndex)</argsstring>
        <name>Cy_BLE_RSCSC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__definitions_1ga24f372ada267f7627ed72ce0640890a5">cy_en_ble_rscs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to the peer device to get characteristic descriptor of the specified characteristic of the Running Speed and Cadence service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service Characteristic. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic descriptor.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Peer device doesn't have a particular descriptor. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the RSCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___r_s_c_s__server__client_1ga9139a40b49e73673f7a2d1b72cefa027">Cy_BLE_RSCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a907f0591c037cc2688f9e9a4de4f0108">CY_BLE_EVT_RSCSC_READ_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__rscs__descr__value__t">cy_stc_ble_rscs_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if the RSCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - In case if the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>).</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - In case if an error occurred with the requested attribute on the peer device, the details are provided with event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1605" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.c" bodystart="1555" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_rscs.h" line="205" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to RSCS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_RSCSC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>