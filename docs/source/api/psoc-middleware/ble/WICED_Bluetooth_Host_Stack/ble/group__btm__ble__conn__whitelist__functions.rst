=========================
Connection and Whitelist
=========================


.. doxygengroup:: btm_ble_conn_whitelist_functions
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: