=========
MultiAdv
=========


.. doxygengroup:: btm_ble_multi_adv_functions
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: