=========================
Generic Attribute (GATT)
=========================


.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__gatt__server__api__functions.rst
   group__gatt__client__api__functions.rst
   group__gatt__common__api.rst

   

.. doxygengroup:: wicedbt_gatt
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
