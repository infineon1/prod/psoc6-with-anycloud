=============================================
wiced_bt_lq_br_edr_stats struct
=============================================

.. doxygenstruct:: wiced_bt_lq_br_edr_stats
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
