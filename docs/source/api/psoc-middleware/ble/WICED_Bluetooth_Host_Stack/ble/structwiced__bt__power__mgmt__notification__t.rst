=============================================
wiced_bt_power_mgmt_notification_t struct
=============================================

.. doxygenstruct:: wiced_bt_power_mgmt_notification_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
