==============================
wiced_bt_public_key_t Struct
==============================

.. doxygenstruct:: wiced_bt_public_key_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: