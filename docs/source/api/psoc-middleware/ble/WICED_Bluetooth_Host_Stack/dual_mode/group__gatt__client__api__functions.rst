===========
Client API
===========
   
.. doxygengroup:: gatt_client_api_functions
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
