<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />Display<sp />2.7<sp />Inch<sp />EPD<sp />(E2271CS021)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />provides<sp />functions<sp />for<sp />supporting<sp />the<sp />2.7"<sp />EPD<sp />Display<sp />(E2271CS021).<sp />This<sp />is<sp />the<sp />same<sp />display<sp />as<sp />used<sp />on<sp />the<sp />CY8CKIT-028-EPD<sp />shield.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">https://www.electronicsdatasheets.com/download/59018c25e34e2457312cc1ab.pdf?format=pdf</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">#<sp />Quick<sp />Start</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[EINK<sp />emWin<sp />application](#eink-emwin-application)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[EINK<sp />driver<sp />usage<sp />without<sp />emWin](#eink-driver-usage-without-emwin)</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">##<sp />EINK<sp />emWin<sp />application</highlight></codeline>
<codeline><highlight class="normal">Follow<sp />the<sp />steps<sp />below<sp />in<sp />order<sp />to<sp />create<sp />a<sp />simple<sp />emWin<sp />application<sp />and<sp />display<sp />some<sp />text<sp />on<sp />it.</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Create<sp />an<sp />empty<sp />application</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Add<sp />this<sp />library<sp />to<sp />the<sp />application</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Add<sp />emWin<sp />library<sp />to<sp />the<sp />application</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Enable<sp />EMWIN_NOSNTS<sp />emWin<sp />library<sp />option<sp />by<sp />adding<sp />it<sp />to<sp />the<sp />Makefile<sp />COMPONENTS<sp />list:</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">COMPONENTS+=EMWIN_NOSNTS</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">5.<sp />place<sp />the<sp />following<sp />code<sp />in<sp />the<sp />main.c<sp />file:</highlight></codeline>
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cyhal.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cybsp.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"GUI.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"mtb_e2271cs021.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"LCDConf.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cy8ckit_028_epd_pins.h"</highlight></codeline>
<codeline />
<codeline><highlight class="normal">cyhal_spi_t<sp />spi;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">const<sp />mtb_e2271cs021_pins_t<sp />pins<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_mosi<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_miso<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_sclk<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_cs<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_CS,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.reset<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_RST,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.busy<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BUSY,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.discharge<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_DISCHARGE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_EN,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.border<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BORDER,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.io_enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_IOEN,</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline />
<codeline><highlight class="normal">uint8_t<sp />previous_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal">uint8_t<sp />*current_frame;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">int<sp />main(void)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />SPI<sp />and<sp />EINK<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_init(&amp;spi,<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,<sp />NC,<sp />NULL,<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CYHAL_SPI_MODE_00_MSB,<sp />false);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />if<sp />(CY_RSLT_SUCCESS<sp />==<sp />result)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_set_frequency(&amp;spi,<sp />20000000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />mtb_e2271cs021_init(&amp;pins,<sp />&amp;spi);</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />current_frame<sp />=<sp />(uint8_t*)LCD_GetDisplayBuffer();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_Init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_SetTextMode(GUI_TM_NORMAL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_SetFont(GUI_FONT_32B_1);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_SetBkColor(GUI_WHITE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_SetColor(GUI_BLACK);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_SetTextAlign(GUI_ALIGN_HCENTER<sp />|<sp />GUI_ALIGN_HCENTER);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_Clear();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />GUI_DispStringAt("Hello<sp />World",<sp />LCD_GetXSize()<sp />/<sp />2,<sp />(LCD_GetYSize()<sp />/<sp />2)<sp />-<sp />(GUI_GetFontSizeY()<sp />/<sp />2));</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />update<sp />the<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />mtb_e2271cs021_show_frame(previous_frame,<sp />current_frame,<sp />MTB_E2271CS021_FULL_4STAGE,<sp />true);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">6.<sp />Build<sp />the<sp />application<sp />and<sp />program<sp />the<sp />kit.</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">##<sp />EINK<sp />driver<sp />usage<sp />without<sp />emWin</highlight></codeline>
<codeline><highlight class="normal">The<sp />EINK<sp />library<sp />can<sp />be<sp />also<sp />used<sp />standalone.</highlight></codeline>
<codeline><highlight class="normal">Follow<sp />the<sp />steps<sp />below<sp />to<sp />create<sp />a<sp />simple<sp />application<sp />which<sp />shows<sp />an<sp />interesting<sp />pattern<sp />on<sp />the<sp />display.</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Create<sp />an<sp />empty<sp />application</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Add<sp />this<sp />library<sp />to<sp />the<sp />application</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Place<sp />the<sp />following<sp />code<sp />in<sp />the<sp />main.c<sp />file:</highlight></codeline>
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cyhal.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cybsp.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cy8ckit_028_epd_pins.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"mtb_e2271cs021.h"</highlight></codeline>
<codeline />
<codeline><highlight class="normal">cyhal_spi_t<sp />spi;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">const<sp />mtb_e2271cs021_pins_t<sp />pins<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_mosi<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_miso<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_sclk<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_cs<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_CS,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.reset<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_RST,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.busy<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BUSY,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.discharge<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_DISCHARGE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_EN,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.border<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_BORDER,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.io_enable<sp />=<sp />CY8CKIT_028_EPD_PIN_DISPLAY_IOEN,</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline />
<codeline><highlight class="normal">uint8_t<sp />previous_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal">uint8_t<sp />current_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline />
<codeline><highlight class="normal">int<sp />main(void)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />i;</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />SPI<sp />and<sp />EINK<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_init(&amp;spi,<sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK,<sp />NC,<sp />NULL,<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CYHAL_SPI_MODE_00_MSB,<sp />false);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />if<sp />(CY_RSLT_SUCCESS<sp />==<sp />result)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_set_frequency(&amp;spi,<sp />20000000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />mtb_e2271cs021_init(&amp;pins,<sp />&amp;spi);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Fill<sp />the<sp />EINK<sp />buffer<sp />with<sp />an<sp />interesting<sp />pattern<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(i<sp />=<sp />0;<sp />i<sp />&lt;<sp />PV_EINK_IMAGE_SIZE;<sp />i++)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />current_frame[i]<sp />=<sp />i<sp />%<sp />0xFF;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Update<sp />the<sp />display<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />mtb_e2271cs021_show_frame(previous_frame,<sp />current_frame,<sp />MTB_E2271CS021_FULL_4STAGE,<sp />true);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for<sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Build<sp />the<sp />application<sp />and<sp />program<sp />the<sp />kit.</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">###<sp />More<sp />information</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/display-epd-e2271cs021/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Code<sp />Examples<sp />using<sp />ModusToolbox<sp />IDE](https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Middleware](https://github.com/cypresssemiconductorco/psoc6-middleware)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[PSoC<sp />6<sp />Resources<sp />-<sp />KBA223067](https://community.cypress.com/docs/DOC-14644)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/libs/display-eink-e2271cs021/README.doxygen.md" />
  </compounddef>
</doxygen>