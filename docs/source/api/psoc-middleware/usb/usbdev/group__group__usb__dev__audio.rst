============
Audio Class
============

.. doxygengroup:: group_usb_dev_audio
   :project: usbdev


.. toctree::

   group__group__usb__dev__audio__macros.rst
   group__group__usb__dev__audio__functions.rst
   group__group__usb__dev__audio__data__structures.rst