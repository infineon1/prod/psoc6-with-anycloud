<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="README_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />RTOS<sp />Abstraction</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />RTOS<sp />abstraction<sp />layer<sp />provides<sp />simple<sp />RTOS<sp />services<sp />like<sp />threads,<sp />semaphores,<sp />mutexes,<sp />queues,<sp />and<sp />timers.<sp />It<sp />is<sp />not<sp />intended<sp />to<sp />be<sp />a<sp />full<sp />features<sp />RTOS<sp />interface,<sp />but<sp />the<sp />provide<sp />just<sp />enough<sp />support<sp />to<sp />allow<sp />for<sp />RTOS<sp />independent<sp />drivers<sp />and<sp />middleware.<sp />This<sp />allows<sp />middleware<sp />applications<sp />to<sp />be<sp />as<sp />portable<sp />as<sp />possible<sp />within<sp />ModusToolbox.<sp />This<sp />library<sp />provides<sp />a<sp />unified<sp />API<sp />around<sp />the<sp />actual<sp />RTOS.<sp />This<sp />allows<sp />middleware<sp />libraries<sp />to<sp />be<sp />written<sp />once<sp />independent<sp />of<sp />the<sp />RTOS<sp />actually<sp />selected<sp />for<sp />the<sp />application.<sp />The<sp />abstraction<sp />layer<sp />provides<sp />access<sp />to<sp />all<sp />the<sp />standard<sp />RTOS<sp />resources<sp />listed<sp />in<sp />the<sp />feature<sp />section<sp />below.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">While<sp />the<sp />primary<sp />purpose<sp />of<sp />the<sp />library<sp />is<sp />for<sp />middleware,<sp />the<sp />abstraction<sp />layer<sp />can<sp />be<sp />used<sp />by<sp />the<sp />application<sp />code.<sp />However,<sp />since<sp />this<sp />API<sp />does<sp />not<sp />provide<sp />all<sp />RTOS<sp />features<sp />and<sp />the<sp />application<sp />generally<sp />knows<sp />what<sp />RTOS<sp />is<sp />being<sp />used,<sp />this<sp />is<sp />typically<sp />an<sp />unnecessary<sp />overhead.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">All<sp />the<sp />RTOS<sp />abstraction<sp />layer<sp />functions<sp />generally<sp />all<sp />work<sp />the<sp />same<sp />way.<sp />The<sp />basic<sp />process<sp />is:</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Include<sp />the<sp />cyabs_rtos.h<sp />header<sp />file<sp />so<sp />that<sp />you<sp />have<sp />access<sp />to<sp />the<sp />RTOS<sp />functions.</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Declare<sp />a<sp />variable<sp />of<sp />the<sp />right<sp />type<sp />(e.g.<sp />cy_mutex_t)</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Call<sp />the<sp />appropriate<sp />create<sp />or<sp />initialize<sp />function<sp />(e.g.<sp />cy_rtos_init_mutex()).<sp />Provide<sp />it<sp />with<sp />a<sp />reference<sp />to<sp />the<sp />variable<sp />that<sp />was<sp />created<sp />in<sp />step<sp />2.</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Access<sp />the<sp />RTOS<sp />object<sp />using<sp />one<sp />of<sp />the<sp />access<sp />functions.<sp />e.g.<sp />cy_rtos_set_mutex().</highlight></codeline>
<codeline><highlight class="normal">5.<sp />If<sp />you<sp />don't<sp />need<sp />it<sp />anymore,<sp />free<sp />up<sp />the<sp />pointer<sp />with<sp />the<sp />appropriate<sp />de-init<sp />function<sp />(e.g.<sp />cy_rtos_deinit_mutex()).</highlight></codeline>
<codeline />
<codeline><highlight class="normal">NOTE:<sp />All<sp />these<sp />functions<sp />need<sp />a<sp />pointer,<sp />so<sp />it<sp />is<sp />generally<sp />best<sp />to<sp />declare<sp />these<sp />"shared"<sp />resources<sp />as<sp />static<sp />global<sp />variables<sp />within<sp />the<sp />file<sp />that<sp />they<sp />are<sp />used.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Getting<sp />Started</highlight></codeline>
<codeline />
<codeline><highlight class="normal">To<sp />use<sp />the<sp />RTOS<sp />Abstraction,<sp />simply<sp />include<sp />a<sp />reference<sp />to<sp />`cyabs_rtos.h`<sp />and<sp />update<sp />the<sp />application's<sp />makefile<sp />to<sp />include<sp />the<sp />appropriate<sp />component.<sp />e.g.<sp />one<sp />of:</highlight></codeline>
<codeline><highlight class="normal">*<sp />COMPONENTS+=RTX</highlight></codeline>
<codeline><highlight class="normal">*<sp />COMPONENTS+=FREERTOS</highlight></codeline>
<codeline><highlight class="normal">*<sp />COMPONENTS+=THREADX</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Features</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />APIs<sp />for<sp />interacting<sp />with<sp />common<sp />RTOS<sp />Features<sp />including:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Threads</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Mutexes</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Semaphores</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Timers</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Queues</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Events</highlight></codeline>
<codeline><highlight class="normal">*<sp />Implementations<sp />are<sp />provided<sp />for</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />FreeRTOS</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />RTX<sp />(CMSIS<sp />RTOS)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />ThreadX</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />RTOS<sp />Configuration<sp />Requirements</highlight></codeline>
<codeline><highlight class="normal">###<sp />FreeRTOS</highlight></codeline>
<codeline><highlight class="normal">To<sp />enable<sp />all<sp />functionality<sp />when<sp />using<sp />with<sp />FreeRTOS,<sp />the<sp />following<sp />configuration<sp />options<sp />must<sp />be<sp />enabled<sp />in<sp />FreeRTOSConfig.h:</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_TRACE_FACILITY</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_MUTEXES</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_RECURSIVE_MUTEXES</highlight></codeline>
<codeline><highlight class="normal">*<sp />configUSE_COUNTING_SEMAPHORES</highlight></codeline>
<codeline><highlight class="normal">*<sp />configSUPPORT_DYNAMIC_ALLOCATION</highlight></codeline>
<codeline><highlight class="normal">*<sp />configSUPPORT_STATIC_ALLOCATION</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Enabling<sp />configSUPPORT_STATIC_ALLOCATION<sp />requires<sp />the<sp />application<sp />to<sp />provide<sp />implementations<sp />for<sp />`vApplicationGetIdleTaskMemory`<sp />and</highlight></codeline>
<codeline><highlight class="normal">`vApplicationGetTimerTaskMemory`functions.<sp />Weak<sp />implementations<sp />for<sp />these<sp />functions<sp />are<sp />provided<sp />as<sp />a<sp />part<sp />of<sp />this<sp />library.<sp />These<sp />can</highlight></codeline>
<codeline><highlight class="normal">be<sp />overridden<sp />by<sp />the<sp />application<sp />if<sp />custom<sp />implementations<sp />of<sp />these<sp />functions<sp />are<sp />desired.&lt;br&gt;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />provides<sp />an<sp />API<sp />`vApplicationSleep`<sp />which<sp />can<sp />be<sp />used<sp />to<sp />enable<sp />tickless<sp />support<sp />in<sp />FreeRTOS.<sp />In<sp />order<sp />to<sp />enable<sp />tickless<sp />mode<sp />with<sp />this<sp />API,<sp />the<sp />following<sp />changes<sp />need<sp />to<sp />be<sp />made<sp />in<sp />`FreeRTOSConfig.h`:</highlight></codeline>
<codeline><highlight class="normal">*<sp />Enables<sp />tickless<sp />mode<sp />with<sp />user<sp />specified<sp />`portSUPPRESS_TICKS_AND_SLEEP`<sp />implementation.&lt;br&gt;</highlight></codeline>
<codeline><highlight class="normal">\c<sp />\#define<sp />`configUSE_TICKLESS_IDLE<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />2`</highlight></codeline>
<codeline><highlight class="normal">*<sp />Hook<sp />`portSUPPRESS_TICKS_AND_SLEEP`<sp />macro<sp />to<sp />`vApplicationSleep`<sp />implementation.&lt;br&gt;</highlight></codeline>
<codeline><highlight class="normal">\c<sp />\#define<sp />`portSUPPRESS_TICKS_AND_SLEEP(<sp />xIdleTime<sp />)<sp /><sp /><sp /><sp />vApplicationSleep(<sp />xIdleTime<sp />)`</highlight></codeline>
<codeline />
<codeline><highlight class="normal">For<sp />further<sp />details<sp />on<sp />Low<sp />power<sp />support<sp />in<sp />FreeRTOS<sp />please<sp />refer<sp />to<sp />documentation<sp />[here](https://www.freertos.org/low-power-tickless-rtos.html)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />RTX<sp />/<sp />ThreadX</highlight></codeline>
<codeline><highlight class="normal">No<sp />specific<sp />requirements<sp />exist</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Porting<sp />Notes</highlight></codeline>
<codeline><highlight class="normal">In<sp />order<sp />to<sp />port<sp />to<sp />a<sp />new<sp />environment,<sp />the<sp />file<sp />cyabs_rtos_impl.h<sp />must<sp />be<sp />provided<sp />with<sp />definitions<sp />of<sp />some<sp />basic<sp />types<sp />for<sp />the<sp />abstraction<sp />layer.<sp /><sp />The<sp />types<sp />expected<sp />to<sp />be<sp />defined<sp />are:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">-<sp />`cy_thread_t`<sp />:<sp />typedef<sp />from<sp />underlying<sp />RTOS<sp />thread<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_thread_arg_t`<sp />:<sp />typedef<sp />from<sp />the<sp />RTOS<sp />type<sp />that<sp />is<sp />passed<sp />to<sp />the<sp />entry<sp />function<sp />of<sp />a<sp />thread.</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_mutex_t`<sp />:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />mutex<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_semaphore_t`:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />semaphore<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_event_t`<sp />:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />event<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_queue_t`<sp />:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />queue<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_timer_callback_arg_t`<sp />:<sp />typedef<sp />from<sp />the<sp />RTOS<sp />type<sp />that<sp />is<sp />passed<sp />to<sp />the<sp />timer<sp />callback<sp />function</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_timer_t`<sp />:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />timer<sp />type</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_time_t`<sp />:<sp />count<sp />of<sp />time<sp />in<sp />milliseconds</highlight></codeline>
<codeline><highlight class="normal">-<sp />`cy_rtos_error_t`<sp />:<sp />typedef<sp />from<sp />the<sp />underlying<sp />RTOS<sp />error<sp />type</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />enum<sp />`cy_thread_priority_t`<sp />needs<sp />to<sp />have<sp />the<sp />following<sp />priority<sp />values<sp />defined<sp />and<sp />mapped<sp />to<sp />RTOS<sp />specific<sp />values:</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_MIN`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_LOW`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_BELOWNORMAL`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_NORMAL`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_ABOVENORMAL`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_HIGH`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_REALTIME`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_PRIORITY_MAX`</highlight></codeline>
<codeline />
<codeline><highlight class="normal">Finally,<sp />the<sp />following<sp />macros<sp />need<sp />to<sp />be<sp />defined<sp />for<sp />memory<sp />allocations:</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_MIN_STACK_SIZE`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_ALIGNMENT`</highlight></codeline>
<codeline><highlight class="normal">-<sp />`CY_RTOS_ALIGNMENT_MASK`</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />More<sp />information</highlight></codeline>
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/abstraction-rtos/html/modules.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="abstraction/rtos/README.doxygen.md" />
  </compounddef>
</doxygen>