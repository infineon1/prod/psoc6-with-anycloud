======================================================
Managing the Makefile for ModusToolbox\ :sup:`®` v2.x
======================================================


.. raw:: html

   <hr>
   
The ModusToolbox build infrastructure creates a project based on
information in a makefile. This document explains how you can manage
the makefile to accomplish common tasks, and it is in the form of
questions and answers. As appropriate, the answer provides a common
use case, what you need to know to get the task done, and an example
snippet. For a more general view of ModusToolbox, including the build
system, refer to the `ModusToolbox User
Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__

This document uses the following terms:

-  *makefile* directory: The folder that contains the makefile; this may
   be a code example (starter application) or your project.

-  *project* directory: The folder created during the project creation
   process; contains all files for the project, including a copy of the
   makefile.

-  *.lib* file: A text file that contains a URL to a GitHub repository
   and a tag or SHA for a specific commit.

-  auto-discovery: The process by which the build infrastructure
   automatically finds files.



When you create a project using the ModusToolbox build system, all
files required by the project are copied into the project directory.
This includes the original makefile found in the code example
(starter application).

**1. What is the makefile and how do I use it?**

The makefile describes how to create and build a project. It contains
information about what files to use, what libraries to include, what
build tools to use, and so forth. This makefile is specific to the
ModusToolbox build system. For general background and reference
information on the command line interface, refer to the `ModusToolbox
User Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__

Cypress provides many code examples
on GitHub, which can be thought of as starter applications. Each
contains a makefile and source files specific to the application. A
code example is not self-contained. When you use the ModusToolbox
IDE’s New Application wizard, or the stand-alone Project Creator
tool, the build system is invoked, and all files required for the
project appear in the project directory. If you use the Eclipse IDE
for ModusToolbox, this folder is in your Eclipse workspace. If you
use the Project Creator, the project directory is wherever you put
it. You can then add the files to your preferred IDE, or work from
the command line.

The makefile controls the project creation and build process. The
original makefile from the code example is duplicated in the project
directory.

**2. Which makefile should I edit, the one in the code example or the
one in the project directory?**

Typically, you create a project from a code example, and then edit
the makefile in your project directory. The changes are local to your
project.

However, you can download a local copy of the code example from
GitHub and edit that makefile. If you edit the makefile in your copy
of the code example, any project built from that example has your
changes.

In other questions and answers, this document refers to the
makefile directory. This could be either your project directory or
a local copy of the code example.




**3. How do I add files to the project so that auto-discovery finds
them?**

The build infrastructure automatically finds all source files in the
makefile directory. This includes .c, .cpp, .h, .hpp, .S,
.s, .o, and .a files.

As a result, a simple solution is to add the files to the *makefile*
directory. The search is recursive, so you can add the files anywhere in
the makefile directory tree and the make build command finds them.

However, the common use case is that you have files that exist outside
the *makefile* directory; for example, a collection of source code on a
company server.

To discover files outside the makefile directory, you can add the path
to the external directory using the CY_EXTAPP_PATH variable.

   CY_EXTAPP_PATH=./../external_source_folder

Use the SOURCES and INCLUDES variables to explicitly specify the source
and header files to the build process from a location not searched by
the build system.

The SOURCES value must be a space-separated list of specific files.
Paths can be absolute or relative to the *makefile* directory to locate
the files. You can create a search pattern with wildcards to find
multiple files.

   SOURCES=$(wildcard ../MySource1/.c) $(wildcard ../MySource2/.c)

Use the INCLUDES variable to specify the folder for header files. The
value is a space-separated list of relative paths to the directories.

   INCLUDES=../MySource1 ../MySource2

You can also specify that certain files or folders should be ignored.
See `How do I make sure auto-discovery ignores certain
files? 

For more information refer to the Section **Adding Source Files** in the
`ModusToolbox User
Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__

**4. How can I add a path that contains spaces?**

Spaces in paths are problematic with many build tools, so avoid them
whenever possible. If you must have spaces in a path, use the
$(wildcard) function to properly escape paths that contain spaces.

   INCLUDES=$(wildcard ../My Source/)

   CY_COMPILER_PATH=$(wildcard C:/Program Files (x86)/IAR
   Systems/Embedded Workbench 7.3/arm/bin)

**5. How can I use an absolute path when the variable value requires a
relative path?**

The files you want to add may live someplace specific on a company
server or your local machine. An absolute path can be easier to manage.
In this case, you can create a make variable configured with an absolute
path and use that as part of your “relative” path.

   ABS_PATH=C:/<path_to_source>/ExternalSourceFiles/

   SOURCES=$(ABS_PATH)/external_source.c




**6. How are slashes handled by the build system?**

On Windows, ModusToolbox uses Cygwin make under the hood. Cygwin is a
POSIX-compatible environment. POSIX paths use forward slash “/” as the
separator for paths. Therefore, “/” must be used when specifying any
path in the makefile.

Windows path names use backslash “\” as the path separator. In cases
when there is a long absolute path and you need to replace “\” with “/”,
you can use the $(subst) function to make the conversion easier.

   WIN_PATH=C:\Users\<some_long_path>\HeaderFiles

   HEADER_FILE=$(subst \\,/,$(WIN_PATH))

   INCLUDES=$(HEADER_FILE)

When specifying a value for ModusToolbox Make variables using the System
Variable in the Environment Variable dialog, make sure you use forward
slashes. For example, to specify the tools directory to be used, you can
make use of the CY_TOOLS_PATHS system variable. Note that the value you
enter must use forward slashes.

**7. How do I make sure auto-discovery ignores certain files?**

For example, you may have a subfolder with files used by a test or
verification system, and you don’t want those compiled into your
executable.

Use the CY_IGNORE variable. The value is a space-separated list of
folders and/or specific files you want ignored by auto-discovery. Use
paths relative to the *makefile* directory. You can create a search
pattern with wildcards to ignore multiple files.

   CY_IGNORE=./filename.c ./TestFolder

   CY_IGNORE+=$(wildcard ./lib/lib1/tests/*.c)

**8. How do I add an external library using a .lib text file?**

The ModusToolbox build infrastructure uses the git version control
system. A .lib file is typically a single line of text that represents a
URL to a git repository, and a specific commit within that repository.
The .lib files provided by Cypress always point to a publicly available
Cypress-owned repository, typically on GitHub. For example, this .lib
file imports the latest version of the Peripheral Driver Library into a
project.

   https://github.com/cypresssemiconductorco/psoc6pdl/#latest-v1.X

To add your own library, create a *.lib* file that points to a git
repository. Place the *.lib* file in the *makefile* directory. For
example:

   https://gitrepos.company.com/mylibrary/#52fac15a7c06ecac951bc67fff94097e9f671efb


The make getlibs command finds and processes all *.lib* files and
uses the git command to clone or pull the code as appropriate. Then,
it checks out the specific hash or tag listed in the *.lib* file. The
source files are copied into the libs folder at the root of the
project folder. The Project Creator and Library Manager tools invoke
make getlibs automatically.

**9. How do I add an external library by specifying files and file
paths?**

See `“How do I add files to the project so that auto-discovery finds
them?” in this document.

**10. How do I add a precompiled binary to my project?**

The build infrastructure automatically finds .o and .a binary files
in the makefile directory. A simple solution is to add the files to
the makefile directory.

However, a common use case is that the binary file(s) exist outside
the makefile directory. Use the LDLIBS variable to specify
application-specific precompiled binaries. The LDLIBS value must be a
space-separated list of specific files. Use paths relative to the
makefile directory to locate the files. You can create a search
pattern with wildcards to find multiple files. These files are
included in the link process.

   LDLIBS=../MyBinaryFolder/binary.o

   LDLIBS+=$(wildcard ../lib/*.a)

If you need to include the header files for the binary, use the
INCLUDES variable. See `How do I add files to the project so that
auto-discovery finds them? 

**11. How do I specify a set of build tools?**

The makefile by default uses GCC , which comes packaged with the
ModusToolbox installer. In the makefile, the combination of variables
TOOLCHAIN and CY_COMPILER_PATH are used to specify the build tools
you want to use.

The other supported toolchains include the ARM Compiler and the IAR
compiler, which must be installed separately.

   The options for the TOOLCHAIN variable include:

-  TOOLCHAIN=GCC_ARM (Uses GCC provided with the Eclipse IDE)

-  TOOLCHAIN=ARM (Uses ARM Compiler)

-  TOOLCHAIN=IAR (Uses IAR Compiler)



Based on the compiler chosen, specify the absolute path to the
compiler’s bin directory using the CY_COMPILER_PATH variable.
   CY_COMPILER_PATH=$(wildcard C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.3/arm/bin) 
   CY_COMPILER_PATH=$(wildcard C:/Keil/ARM/ARMCC/bin)




**12. How do I tell the build system where custom build tools are?**

To use a custom toolchain that is not directly supported by
ModusToolbox, follow these steps:

       (1) In your project directory, navigate to
       libs\psoc6make\make\toolchains\. Note: These files do not exist
       in the original code example folder. They are added to the
       project by the build system when you create the project.

      (2) Create a new file <NEW_TOOLCHAIN>.mk and add the toolchain
       configuration.

       (3) In the BSP directory, navigate to TARGET_<BSP>\linker\\ and create a
       directory named TOOLCHAIN_<NEW_TOOLCHAIN>. Add the custom linker
       script inside this directory. Note that the prefix TOOLCHAIN\_
       is important.

      (4) In the BSP directory, navigate to TARGET_<BSP>\startup\\ and create
       a directory named TOOLCHAIN_<NEW_TOOLCHAIN>. Add the custom
       startup file inside this directory.

      (5) Follow the steps mentioned in `How do I specify a set of build
       tools <#page4>`__ to add the toolchain and the compiler path.
       Make sure the value you provide for TOOLCHAIN is same as the name
       of the mk file you created in the Step 2.

      (6) Build the application.

ModusToolbox BSPs include startup and linker files for supported
toolchains. If your toolchain requires different files, you need to
provide them.

**13. How do I pass compiler flags?**

Use the CFLAGS and CXXFLAGS variables to add additional C and C++
compiler flags respectively. Refer to the respective toolchain
documentation for various options.

Example: For the GNU ARM Toolchain, the various options for the compiler
can be found here. They
can be added as follows:

   CFLAGS=-Wall -O2

   CXXFLAGS=finline-functions

The value of each variable is a space-separated list of flags supported
by the compiler.

To view the changes taking effect, set the variable VERBOSE to ‘1’ in
the makefile. This displays the full command lines when building.

Precisely where the compiler flags appear depends upon the toolchain or
IDE in use. You can also specify the VERBOSE option using the IDE
instead of the makefile. For example, in the ModusToolbox IDE, open the
project properties and go to the C/C++ Build panel. Here, you can
specify VERBOSE=1 in the Build command item as shown in Figure
1. 


**Figure 1. Build Command**

|image1|

Click Apply and Close to save your changes and then build the
application. Note that changes made to the IDE options are not reflected
in the makefile.

Similarly, when using the command line, you can specify “make build
VERBOSE=1” to display the full command lines when building.

**14. How do I create a custom build configuration?**

By default, ModusToolbox provides Debug and Release build configurations
for different toolchains. They are defined in the respective
<TOOLCHAIN>.mk file in libs\psoc6make\make\toolchains in the project
folder. To create your own build configuration, set the CONFIG variable
to a value. For example:

   CONFIG=MyBuild

The build system also uses the value in the CONFIG variable to generate
the name of the output directory inside the project folder on this path:
build\<BSP>\\ .





Do not set this variable to a null or empty value.

**15. How do I pass linker flags?**

Use the LDFLAGS variable to add custom linker flags. Refer to the
respective toolchain documentation for various options.

Example: For the GNU ARM Toolchain, the various options for the linker
can be found here.

LDFLAGS=-nodefaultlibs

The values must be space-separated.

**16. How do I use a custom linker script?**

The build infrastructure automatically uses default linker scripts.
Default linker scripts are provided in the board support package for all
supported toolchains (Arm, GCC, and IAR).

To use a custom linker script, specify the file in the LINKER_SCRIPT
variable. The value must be a relative path from the makefile to the
linker script.

   LINKER_SCRIPT=../../MyFiles/myLinkerScript.ld

The script must be compatible with your toolchain (.sct, .ld, .icf).

**17. How do I specify a pre-build or post-build task?**

Refer to the **Section Pre-builds and Post-builds** in the `ModusToolbox
User Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__

**18. How do I build a static library?**

The build system supports creating either an executable or a library. If
the project builds an executable, it has the APPNAME variable set, and
the LIBNAME variable is empty.

To build a library file, ensure that the APPNAME variable is empty, and
specify the library name in LIBNAME.

**19. What is a COMPONENT?**

A COMPONENT is a folder that contains a collection of files. Typically,
a component consists of files that implement some feature or
functionality in an application. For example, in a Cypress BSP, the
hardware design is encapsulated in a COMPONENT named BSP_DESIGN_MODUS.
It contains the design.modus file and may contain other hardware
configuration files associated with the BSP.

You can add or disable components.


**20. How do I create a custom COMPONENT and add it to the
application?**

Create a folder named “COMPONENT\_<NAME>”. It can be created in any
location found by auto-discovery. Put the files for that component in
the folder. Note that the folder must have the prefix COMPONENT\_.

You then add the component to your application using COMPONENTS+=<NAME>.
Note that the folder prefix is not used. For example, if you created a
folder named COMPONENT_MY_COMPONENT, to add that component to the
application you would do this in the makefile:

   COMPONENTS+=MY_COMPONENT

**21. How do I disable a COMPONENT?**

Use DISABLE_COMPONENTS+=<NAME> to disable a component. For example:

DISABLE_COMPONENTS+=MY_COMPONENT

**22. How do I override the default design.modus file in a BSP?**

You may wish to have a custom hardware configuration for your project.
The design.modus file is part of the default BSP_DESIGN_MODUS component.
To do this, you disable the default component and add a custom
component.

   # Disable default component

   DISABLE_COMPONENTS+=BSP_DESIGN_MODUS

-  Add my custom component

..

   COMPONENTS+=CUSTOM_DESIGN_MODUS

Detailed information about how to do this is in the “Overriding the BSP
Configuration Files” section of the `ModusToolbox User
Guide. <http://www.cypress.com/ModusToolboxUserGuide>`__

**23. How do I create my own BSP?**

You need a custom BSP when your project does not use a Cypress kit.

Detailed information about how to do this is in the “Creating a BSP for
your Board” section in the `ModusToolbox User
Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__ For a BSP used
for the Bluetooth SDK, the information is in the “Custom BSP” section in
the `ModusToolbox User
Guide. <https://www.cypress.com/ModusToolboxUserGuide>`__

**24. How do I use a custom BSP for my application?**

You are likely to create a project for hardware that is not a Cypress
kit. You use a custom BSP to support your board. The custom BSP is in a
folder named TARGET_<BSP_NAME>. In the makefile, you set the value of
the TARGET make variable to match your custom BSP.

For example, if your BSP is named “MyBoard”, the contents of the BSP
must be in a folder named TARGET\_ MyBoard, and your makefile looks like
this:




-  Target board/hardware 

   TARGET=MyBoard

When you execute make getlibs, your BSP files appear in a folder named
TARGET_MyBoard inside your project.


Related Categories:

Keywords: ModusToolbox Eclipse Import Code Example

Product Family: PSoC 6 MCU

Related Tags: [Select the Tags by clicking the checkbox associated to
the Tags]

.. |SQUAR|  raw:: html
   
   &#9744;
   
.. |CRSQR|  raw:: html
   
   &#9746;

Clocks & Buffers Tags
======================

+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Adapter  | |SQUAR| Algorithm    | |SQUAR| Bitmap   | |SQUAR| Buffer   | |SQUAR| Bypass   | |SQUAR| CLKMAKER   |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| CML      | |SQUAR| CY3670       | |SQUAR| CY3672   | |SQUAR| CY3675   | |SQUAR| CY36800  | |SQUAR| Cascade    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Charge   | |SQUAR| Clock        | |SQUAR| Clocks   | |SQUAR| Clocks   | |SQUAR| ComLink  | |SQUAR| Crystal    |
| Pump             | Tree                 |                  | and              |                  | Osc                |
|                  |                      |                  | Buffers          |                  | illators           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| CyClock  | |SQUAR| CyClo        | |SQUAR| Cyb      | |SQUAR| Cyb      | |SQUAR| Cycle to | |SQUAR| DCXO       |
|                  | ckWizard             | erClocks         | erClocks         | Cycle            |                    |
|                  |                      |                  | Online           |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Delay    | |SQUAR| Deter        | |SQUAR| Diff     | |SQUAR| Divider  | |SQUAR| Duty     | |SQUAR| EMI        |
|                  | ministic             | erential         |                  | Cycle            |                    |
|                  | Jitter               |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Enhanced | |SQUAR| Ent          | |SQUAR| Entrant  | |SQUAR| FNOM     | |SQUAR| Factory  | |SQUAR| Failsafe   |
| Per              |                      |                  |                  |                  |                    |
| formance         |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Fanout   | |SQUAR| FastEdge     | |SQUAR| Feedback | |SQUAR| Field    | |SQUAR| Flexo    | |SQUAR| F          |
|                  |                      |                  |                  |                  | requency           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| F        | |SQUAR| Function     | |SQUAR| Ge       | |SQUAR| Ground   | |SQUAR| HPB      | |SQUAR| HSTL       |
| requency         | Select               | nerators         | Bounce           |                  |                    |
| M                |                      |                  |                  |                  |                    |
| argining         |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Hershey  | |SQUAR| I2C          | |SQUAR| IBIS     | |SQUAR| I        | |SQUAR| Input    | |SQUAR| In         |
| Kiss             |                      | Model            | mpedance         |                  | staClock           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Inverted | |SQUAR| Jed          | |SQUAR| Jedec    | |SQUAR| Jitter   | |SQUAR| LVCMOS   | |SQUAR| LVDS       |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| LVPECL   | |SQUAR| Layout       | |SQUAR| Lexmark  | |SQUAR| Loop     | |SQUAR| MoBL     | |SQUAR| Mo         |
|                  |                      | Profile          | B                |                  | dulation           |
|                  |                      |                  | andwidth         |                  | Rate               |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Mu       | |SQUAR| NZDB         | |SQUAR| Non-     | |SQUAR| OTP      | |SQUAR| Offset   | |SQUAR| Os         |
| ltiplier         |                      | Volatile         |                  |                  | cillator           |
|                  |                      | Memory           |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Output   | |SQUAR| Ove          | |SQUAR| PCI      | |SQUAR| PLL      | |SQUAR| PPM      | |SQUAR| PREMIS     |
|                  | rvoltage             | Express          |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Period   | |SQUAR| Phase        | |SQUAR| Phase    | |SQUAR| Power    | |SQUAR| Program  | |SQUAR| Prog       |
|                  |                      | Noise            | Supply           |                  | rammable           |
|                  |                      |                  | Noise            |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Pro      | |SQUAR| Pul          | |SQUAR| RMS      | |SQUAR| Rambus   | |SQUAR| Random   | |SQUAR| R          |
| pagation         | lability             |                  |                  | Jitter           | eference           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Rise     | |SQUAR| R            | |SQUAR| S        | |SQUAR| Serial   | |SQUAR| Signal   | |SQUAR| Skew       |
| Fall             | oboClock             | chematic         |                  | I                |                    |
| Time             |                      |                  |                  | ntegrity         |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Socket   | |SQUAR| S            | |SQUAR| Spread % | |SQUAR| Spread   | |SQUAR| Spread   | |SQUAR| Spread     |
|                  | pecialty             |                  | Aware            | Profile          | Spectrum           |
|                  | Clocks               |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Syn      | |SQUAR| TCXO         | |SQUAR| TIE      | |SQUAR| Ter      | |SQUAR| Thermal  | |SQUAR| Tr         |
| thesizer         |                      |                  | mination         |                  | anslator           |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Unde     | |SQUAR| UniClock     | |SQUAR| Unit     | |SQUAR| VCO      | |SQUAR| VCXO     | |SQUAR| Volatile   |
| rvoltage         |                      | Interval         |                  |                  | Memory             |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Voltage  | |SQUAR| XCG          | |SQUAR| XDR      | |SQUAR| XO       | |SQUAR| Xtal     | |SQUAR| ZDB        |
| Droop            |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+
| |SQUAR| Zepto    |                      |                  |                  |                  |                    |
+------------------+----------------------+------------------+------------------+------------------+--------------------+

**Lighting & Power Control Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Board    | |SQUAR| Boost    | |SQUAR| Buck     | |SQUAR| CY3261   | |SQUAR| CY3267   | |SQUAR| CY3268   |
| Layout           | R                | R                |                  |                  |                  |
|                  | egulator         | egulator         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3269   | |SQUAR| Color    | |SQUAR| Current  | |SQUAR| DALI     | |SQUAR| DMX      | |SQUAR| FN Pins  |
|                  | Mixing           | Sense            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HB LEDs  | |SQUAR| Hy       | |SQUAR| MOSFETs  | |SQUAR| MPPT     | |SQUAR| Mo       | |SQUAR| PrISM    |
|                  | steretic         |                  |                  | dulators         |                  |
|                  | Co               |                  |                  |                  |                  |
|                  | ntroller         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pro      | |SQUAR| SSDM     | |SQUAR| Sc       | |SQUAR| S        | |SQUAR| Trip     | |SQUAR| Voltage  |
| gramming         |                  | hematics         | witching         |                  | R                |
|                  |                  |                  | Re               |                  | egulator         |
|                  |                  |                  | gulators         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Wireless/RF Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 2.4 GHz  | |SQUAR| 8DR      | |SQUAR| Antenna  | |SQUAR| Audio    | |SQUAR| AgileHID | |SQUAR| Bridge   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3271   | |SQUAR| CY3630   | |SQUAR| CY4636   | |SQUAR| CY4672   | |SQUAR| CYFI     | |SQUAR| CYFISNP  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CYRF6936 | |SQUAR| CYRF7936 | |SQUAR| Channel  | |SQUAR| DDR      | |SQUAR| DSSS     | |SQUAR| GFSK     |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HID      | |SQUAR| HUB      | |SQUAR| IRQ      | |SQUAR| Inte     | |SQUAR| Keyboard | |SQUAR| Link     |
|                  |                  |                  | rference         |                  | Budget           |
|                  |                  |                  | A                |                  |                  |
|                  |                  |                  | voidance         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mouse    | |SQUAR| Node     | |SQUAR| PMU      | |SQUAR| PN Code  | |SQUAR| PRoC     | |SQUAR| PRoC-CS  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PRoC-EMB | |SQUAR| PRoC-TT  | |SQUAR| PRoC-USB | |SQUAR| PRoC-UI  | |SQUAR| Power    | |SQUAR| Preamble |
|                  |                  |                  |                  | A                |                  |
|                  |                  |                  |                  | mplifier         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pseudo   | |SQUAR| RF       | |SQUAR| RSSI     | |SQUAR| Range    | |SQUAR| Remote   | |SQUAR| SCD      |
| Noise            |                  |                  |                  |                  |                  |
| code             |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SDR      | |SQUAR| SOP      | |SQUAR| SPIM     | |SQUAR| Star     | |SQUAR| S        | |SQUAR| Trackpad |
|                  |                  |                  | -Network         | treaming         |                  |
|                  |                  |                  | Protocol         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless | |SQUAR| Wireless |
|                  | USB              | USB LP           | USB LS           | USB LR           | USB NL           |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Wireless |                  |                  |                  |                  |                  |
| Ca               |                  |                  |                  |                  |                  |
| pacitive         |                  |                  |                  |                  |                  |
| Touch            |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Memory Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Address  | |SQUAR| Async    | |SQUAR| Au       | |SQUAR| A        | |SQUAR| BHE /    | |SQUAR| BUSY     |
| Bus              |                  | tomotive         | utostore         | BLE              |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Battery  | |SQUAR| Burst    | |SQUAR| Bus      | |SQUAR| Clock    | |SQUAR| DDR      | |SQUAR| Data Bus |
| Backup           |                  | Co               |                  |                  |                  |
|                  |                  | ntention         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Data     | |SQUAR| Data     | |SQUAR| D        | |SQUAR| Dual     | |SQUAR| Echo     | |SQUAR| FIFO     |
| I                | R                | atasheet         | Port             | Clocks           |                  |
| ntegrity         | etention         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FPGA     | |SQUAR| Flags    | |SQUAR| Flo      | |SQUAR| Fullflex | |SQUAR| FRAM     | |SQUAR| HOLD     |
|                  |                  | wthrough         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HD-FIFO  | |SQUAR| HSB      | |SQUAR| I2C      | |SQUAR| INT      | |SQUAR| I        | |SQUAR| I        |
|                  |                  |                  |                  | mpedance         | nterface         |
|                  |                  |                  |                  | Matching         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Inte     | |SQUAR| JTAG     | |SQUAR| Junction | |SQUAR| Low      | |SQUAR| MSL      | |SQUAR| M        |
| rleaving         |                  | Tem              | Power            |                  | igration         |
|                  |                  | perature         |                  |                  | Path             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| MoBL     | |SQUAR| Models   | |SQUAR| NoBL     | |SQUAR| ODT      | |SQUAR| Obsolete | |SQUAR| PCB      |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PLL /    | |SQUAR| P        | |SQUAR| Parallel | |SQUAR| Parity   | |SQUAR| Part     | |SQUAR| Part     |
| DLL              | ackaging         |                  |                  | Decoder          | Di               |
|                  |                  |                  |                  |                  | fference         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Password | |SQUAR| Pin      | |SQUAR| Pipeline | |SQUAR| Power    | |SQUAR| Power On | |SQUAR| P        |
| Pr               | Confi            |                  | Con              | State            | rocessor         |
| otection         | guration         |                  | sumption         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| QDR      | |SQUAR| Quali    | |SQUAR| Quality  | |SQUAR| RECALL   | |SQUAR| RTC      | |SQUAR| Race     |
|                  | fication         |                  |                  |                  | C                |
|                  | Reports          |                  |                  |                  | ondition         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Read     | |SQUAR| SER      | |SQUAR| SPCM     | |SQUAR| SPI      | |SQUAR| SRAM     | |SQUAR| Serial   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Signal   | |SQUAR| Sync     | |SQUAR| Te       | |SQUAR| Ter      | |SQUAR| Timing   | |SQUAR| Vcap     |
| I                |                  | chnology         | mination         |                  |                  |
| ntegrity         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Vddq     | |SQUAR| Voltage  | |SQUAR| Vref     | |SQUAR| Width /  | |SQUAR| Write    | |SQUAR| nvSRAM   |
|                  | Levels           |                  | Depth            |                  |                  |
|                  |                  |                  | E                |                  |                  |
|                  |                  |                  | xpansion         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| uPower   |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Interface Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 10B/8B   | |SQUAR| 3 Level  | |SQUAR| ALDEC    | |SQUAR| ATM      | |SQUAR| Altera   | |SQUAR| BIST     |
| Decoder          | Inputs           |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| BSDL     | |SQUAR| Biasing  | |SQUAR| CDR      | |SQUAR| CML      | |SQUAR| CPLD     | |SQUAR| CY3900i  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3950i  | |SQUAR| CY       | |SQUAR| Cable    | |SQUAR| Cable    | |SQUAR| Channel  | |SQUAR| Clock    |
|                  | USBISRPC         |                  | Driver           | Bonding          | Mu               |
|                  |                  |                  |                  |                  | ltiplier         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Clocking | |SQUAR| Coaxial  | |SQUAR| Copper   | |SQUAR| Coupling | |SQUAR| Crystal  | |SQUAR| Current  |
| Modes            | Cable            | Cable            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| DVB      | |SQUAR| Data     | |SQUAR| Data     | |SQUAR| Delta39K | |SQUAR| Des      | |SQUAR| Dese     |
|                  | C                | rate             |                  | erialize         | rializer         |
|                  | haracter         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Dual     | |SQUAR| EPLD     | |SQUAR| EPROM    | |SQUAR| ESCON    | |SQUAR| El       | |SQUAR| E        |
| Channel          |                  |                  |                  | asticity         | qualizer         |
|                  |                  |                  |                  | Buffer           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Error    | |SQUAR| Ev       | |SQUAR| FPGA     | |SQUAR| Fiber    | |SQUAR| F        | |SQUAR| Framer   |
|                  | aluation         |                  | Optics           | lash370i         |                  |
|                  | Board            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Framing  | |SQUAR| Framing  | |SQUAR| Gigabit  | |SQUAR| Hex      | |SQUAR| High     | |SQUAR| Hotlink  |
| C                | Mode             | Ethernet         |                  | Speed            |                  |
| haracter         |                  |                  |                  | Serial           |                  |
|                  |                  |                  |                  | Links            |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HotlinkI | |SQUAR| H        | |SQUAR| IP       | |SQUAR| Isr      | |SQUAR| Jtag     | |SQUAR| LFI      |
|                  | otlinkII         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LVTTL    | |SQUAR| Logic    | |SQUAR| Loop     | |SQUAR| M        | |SQUAR| Max340   | |SQUAR| Model    |
|                  | Level            | Back             | acrocell         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Non-     | |SQUAR| OC-1     | |SQUAR| OC-2     | |SQUAR| OC-3     | |SQUAR| O        | |SQUAR| PAL      |
| Volatile         |                  |                  |                  | perating         |                  |
|                  |                  |                  |                  | System           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PECL     | |SQUAR| PLD      | |SQUAR| PLL      | |SQUAR| Parallel | |SQUAR| Parallel | |SQUAR| Parity   |
|                  |                  |                  | Input            | Output           |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Phase    | |SQUAR| PoF      | |SQUAR| Point To | |SQUAR| Point To | |SQUAR| Power    | |SQUAR| Pro      |
| Align            |                  | Multi            | Point            | Supply           | gramming         |
| Buffer           |                  | Point            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Quad     | |SQUAR| Qu       | |SQUAR| Receiver | |SQUAR| R        | |SQUAR| R        | |SQUAR| R        |
| Channel          | antum38K         |                  | ecovered         | edundant         | eference         |
|                  |                  |                  | Clock            | Outputs          | Clock            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Report   | |SQUAR| Reprog   | |SQUAR| SMA      | |SQUAR| SMPTE    | |SQUAR| SM       | |SQUAR| SM       |
| File             | rammable         |                  |                  | PTE-259M         | PTE-292M         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SONET    | |SQUAR| SPICE    | |SQUAR| SPLD     | |SQUAR| STAPL    | |SQUAR| SVF      | |SQUAR| Serial   |
|                  |                  |                  |                  |                  | Input            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Serial   | |SQUAR| Se       | |SQUAR| S        | |SQUAR| Size     | |SQUAR| Special  | |SQUAR| Speci    |
| Output           | rializer         | imulator         |                  | C                | fication         |
|                  |                  |                  |                  | haracter         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| TTL      | |SQUAR| Tem      | |SQUAR| Third    | |SQUAR| Tra      | |SQUAR| Tra      | |SQUAR| USBISRPC |
|                  | perature         | Party            | nsceiver         | nsmitter         | Cable            |
|                  |                  | Tool             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ultra37K | |SQUAR| User     | |SQUAR| VHDL     | |SQUAR| Verilog  | |SQUAR| Voltage  | |SQUAR| Warp     |
|                  | Guide            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Word     |                  |                  |                  |                  |                  |
| Sync             |                  |                  |                  |                  |                  |
| Sequence         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**PSoC 1 Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ADC      | |SQUAR| ADCINC   | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| API      | |SQUAR| Assembly |
|                  |                  |                  | Bus              |                  | Language         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Boot.asm | |SQUAR| Bo       | |SQUAR| BSDL     | |SQUAR| Build    | |SQUAR| Cal      | |SQUAR| Capsense |
|                  | otloader         | Files            | Errors           | ibration         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Clock    | |SQUAR| Clock    | |SQUAR| Cloning  | |SQUAR| Column   | |SQUAR| Commu    | |SQUAR| Co       |
|                  | Synchro          |                  | Clock            | nication         | mparator         |
|                  | nization         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Compiler | |SQUAR| Counter  | |SQUAR| CPU      | |SQUAR| Crystal  | |SQUAR| CT Block | |SQUAR| DAC      |
|                  |                  | Speed            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| D        | |SQUAR| D        | |SQUAR| Delta    | |SQUAR| Dev      | |SQUAR| Digital  | |SQUAR| Dynamic  |
| ebugging         | ecimator         | Sigma            | elopment         |                  | Reconfi          |
|                  |                  | ADC              | Kit              |                  | guration         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ECO      | |SQUAR| EEPROM   | |SQUAR| Errata   | |SQUAR| Filter   | |SQUAR| Flash    | |SQUAR| Flash    |
|                  |                  |                  |                  |                  | Security         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Global   | |SQUAR| GPIO     | |SQUAR| Hex File | |SQUAR| I2C      | |SQUAR| I2C-USB  | |SQUAR| ICE Cube |
| R                |                  |                  |                  | Bridge           |                  |
| esources         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Inst     | |SQUAR| Internet | |SQUAR| I        | |SQUAR| ISR      | |SQUAR| ISSP     | |SQUAR| Large    |
| allation         | Explorer         | nterrupt         |                  |                  | Memory           |
|                  |                  |                  |                  |                  | Model            |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LCD      | |SQUAR| License  | |SQUAR| MAC      | |SQUAR| M        | |SQUAR| M        | |SQUAR| Mux      |
|                  |                  |                  | iniProg1         | iniProg3         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mux Bus  | |SQUAR| OCD      | |SQUAR| Offset   | |SQUAR| Pod      | |SQUAR| Port and | |SQUAR| Power    |
|                  |                  |                  |                  | Pins             | Ma               |
|                  |                  |                  |                  |                  | nagement         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Pr       | |SQUAR| PWM      | |SQUAR| RAM      | |SQUAR| RTC      | |SQUAR| SAR      | |SQUAR| SC Block |
| oduction         |                  |                  |                  |                  |                  |
| Pr               |                  |                  |                  |                  |                  |
| ogrammer         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SMP      | |SQUAR| SPI      | |SQUAR| System   | |SQUAR| Timer    | |SQUAR| UART     | |SQUAR| USB      |
|                  |                  | Level            |                  |                  |                  |
|                  |                  | Design           |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| USBUART  | |SQUAR| Watchdog | |SQUAR| A        | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| Build    |
|                  |                  | mplifier         | R                | UM               | Tools            |
|                  |                  | UM               | eference         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Commu    | |SQUAR| CRC UM   | |SQUAR| CYFI     | |SQUAR| Device   | |SQUAR| Digital  | |SQUAR| DTMF     |
| nication         |                  |                  | Pro              | UM               |                  |
| UM               |                  |                  | gramming         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Fan      | |SQUAR| Firmware | |SQUAR| FMEA     | |SQUAR| Port     | |SQUAR| PSoC     | |SQUAR| Voltage  |
| Co               | UM               |                  | Expander         | Power            | S                |
| ntroller         |                  |                  |                  | System           | equencer         |
| UM               |                  |                  |                  | Arch             | UM               |
|                  |                  |                  |                  | itecture         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**PSoC 3/4/5 Tags**

+-------------------+------------------+------------------+------------------+------------------+------------------+
|**Component        |                  |                  |                  |                  |                  |
|tags**             |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Analog    | |SQUAR| Analog   | |SQUAR| Boost    | |SQUAR| Bo       | |SQUAR| CAN      | |SQUAR| CapS     |
| Hardware          | Mux              | C                | otloader         |                  | ense_CSD         |
| Mux               |                  | onverter         | /                |                  |                  |
|                   |                  |                  | Boot             |                  |                  |
|                   |                  |                  | loadable 	     |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| C         | |SQUAR| Clock    | |SQUAR| Co       | |SQUAR| Control  | |SQUAR| Counter  | |SQUAR| CRC      |
| haracter          |                  | mparator         | / Status         |                  |                  |
| LCD               |                  |                  | Register         |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| DAC       | |SQUAR| D        | |SQUAR| Delta    | |SQUAR| DFB      | |SQUAR| DFB      | |SQUAR| Die      |
|                   | ebouncer         | Sigma            |                  | A                | Tem              |
|                   |                  | ADC              |                  | ssembler         | perature         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Digital   | |SQUAR| Digital  | |SQUAR| DMA      | |SQUAR| EEPROM   | |SQUAR| emFile   | |SQUAR| EMIF     |
| Co                | Mul              |                  |                  | SPI Mode         |                  |
| mparator          | tiplexer         |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EzI2C     | |SQUAR| Fan      | |SQUAR| Filter   | |SQUAR| F        | |SQUAR| Glitch   | |SQUAR| Global   |
| Slave             | Co               |                  | requency         | Filter           | Signal           |
|                   | ntroller         |                  | Divider          |                  | R                |
|                   |                  |                  |                  |                  | eference         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Graphic   | |SQUAR| I2C /I   | |SQUAR| iAP      | |SQUAR| I        | |SQUAR| LIN      | |SQUAR| Logic    |
| LCD               | 2S               |                  | nterrupt         |                  | Gates            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Lookup    | |SQUAR| Manual   | |SQUAR| MDIO     | |SQUAR| Mixer    | |SQUAR| Opamp    | |SQUAR| PGA      |
| Table             | Routing          |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ports     | |SQUAR| Power    | |SQUAR| PRS      | |SQUAR| PWM      | |SQUAR| Qu       | |SQUAR| R        |
| and Pins          | Monitor          |                  |                  | adrature         | esistive         |
|                   |                  |                  |                  | Decoder          | Touch            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| RTC       | |SQUAR| RTD      | |SQUAR| Sample / | |SQUAR| SAR ADC  | |SQUAR| SAR      | |SQUAR| Segment  |
|                   | Ca               | Track            |                  | S                | LCD              |
|                   | lculator         | and Hold         |                  | equencer         |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SGPIO     | |SQUAR| Shift    | |SQUAR| Sleep    | |SQUAR| SM /     | |SQUAR| SPDIF    | |SQUAR| SPI      |
|                   | Register         | Timer            | PMBus            |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Sync      | |SQUAR| Th       | |SQUAR| Ther     | |SQUAR| TIA      | |SQUAR| Timer    | |SQUAR| TMP05    |
|                   | ermistor         | mocouple         |                  |                  | I                |
|                   | Ca               | Ca               |                  |                  | nterface         |
|                   | lculator         | lculator         |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Tr        | |SQUAR| UART     | |SQUAR| UDBClkEn | |SQUAR| USBFS    | |SQUAR| USBMIDI  | |SQUAR| USBUART  |
| imMargin          |                  |                  |                  |                  | (CDC             |
|                   |                  |                  |                  |                  | In               |
|                   |                  |                  |                  |                  | terface)         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Voltage   | |SQUAR| Voltage  | |SQUAR| Vref     | |SQUAR| WaveDAC  |                  |                  |
| Fault             | S                |                  |                  |                  |                  |
| Detector          | equencer         |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
|**General          |                  |                  |                  |                  |                  |
|Tags**             |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Analog    | |SQUAR| Analog   | |SQUAR| Analog   | |SQUAR| API      | |SQUAR| App      | |SQUAR| Assembly |
| Bus               | Global           | Mux Bus          |                  | lication         | Language         |
|                   | Bus              |                  |                  | Specific         |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Bo        | |SQUAR| Boundary | |SQUAR| Bridge   | |SQUAR| Build    | |SQUAR| Clock    | |SQUAR| Compiler |
| otloader          | Scan /           | Control          | Settings         |                  | - GCC            |
| Host              | BSDL             | Panel            |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Compiler  | |SQUAR| Compiler | |SQUAR| Compiler | |SQUAR| C        | |SQUAR| C        | |SQUAR| Creator  |
| - KEIL            | - MDK            | - RVDS           | ortex-M0         | ortex-M3         | Regi             |
|                   |                  |                  |                  |                  | stration         |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Custom    | |SQUAR| Datapath | |SQUAR| D        | |SQUAR| DMA      | |SQUAR| DVK      | |SQUAR| ECO      |
| C                 | Confi            | ebugging         | Wizard           |                  |                  |
| omponent          | guration         |                  |                  |                  |                  |
| Inte              | Tool             |                  |                  |                  |                  |
| rconnect          |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Errata    | |SQUAR| Error    | |SQUAR| Flash    | |SQUAR| Hex File | |SQUAR| Inst     | |SQUAR| ISSP /   |
|                   | Message          |                  |                  | allation         | HSSP             |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| KEIL      | |SQUAR| Linux    | |SQUAR| Low      | |SQUAR| LVD /    | |SQUAR| MFi      | |SQUAR| M        |
| Regi              | Platform         | Power            | HVD              |                  | iniProg3         |
| stration          |                  | Modes            |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Opti      | |SQUAR| Pr       | |SQUAR| PSoC     | |SQUAR| PSoC     | |SQUAR| Reset    | |SQUAR| RTOS     |
| mization          | ogrammer         | Creator          | Pr               |                  |                  |
|                   | COM              |                  | ogrammer         |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S         | |SQUAR| Silicon  | |SQUAR| Software | |SQUAR| STA      | |SQUAR| Supply   | |SQUAR| System   |
| chematic          |                  | Download         |                  | Voltage          | R                |
|                   |                  |                  |                  |                  | eference         |
|                   |                  |                  |                  |                  | Guide            |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Verilog   | |SQUAR| Watchdog | |SQUAR| Windows  |                  |                  |                  |
|                   |                  | Platform         |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| **Kit             |                  |                  |                  |                  |                  |
| Tags**            |                  |                  |                  |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CAN /     | |SQUAR| CapSense | |SQUAR| CY8      | |SQUAR| CY8      | |SQUAR| CY8      | |SQUAR| Digital  |
| LIN EBK           | E                | CKIT-001         | CKIT-030         | CKIT-042         | Audio            |
|                   | xpansion         | Kit              | / 050            | Kit              | EBK              |
|                   | EBK              |                  | Kit              |                  |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| First     | |SQUAR| LCD      | |SQUAR| MFI EBK  | |SQUAR| Power    | |SQUAR| PSoC     | |SQUAR| Thermal  |
| Touch             | Segment          |                  | Su               | 3/4/5            | Ma               |
| Kit               | Drive            |                  | pervisor         | P                | nagement         |
|                   | EBK              |                  | EBK              | rocessor         | EBK              |
|                   |                  |                  |                  | Module           |                  |
+-------------------+------------------+------------------+------------------+------------------+------------------+

**Touch Sensing Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| ADC      | |SQUAR| Air gap  | |SQUAR| Back     | |SQUAR| Bleeder  | |SQUAR| Bo       | |SQUAR| CMOD     |
|                  |                  | lighting         | Resistor         | otloader         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CSA      | |SQUAR| CSD      | |SQUAR| CSD      | |SQUAR| CSD2X    | |SQUAR| CSDADC   | |SQUAR| CSDAUTO  |
|                  |                  | Pa               |                  |                  |                  |
|                  |                  | rameters         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3203A  | |SQUAR| CY3213A  | |SQUAR| CY3214   | |SQUAR| CY3218   | |SQUAR| CY32     | |SQUAR| CY32     |
|                  |                  |                  |                  | 80-20x34         | 80-20xx6         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY32     | |SQUAR| C        | |SQUAR| C        | |SQUAR| C        | |SQUAR| CY       | |SQUAR| C        |
| 80-21x34         | Y8C20x34         | Y8C20xx6         | Y8C21x34         | 8C21xxx-         | Y8C24x94         |
|                  |                  |                  |                  | CapSense         |                  |
|                  |                  |                  |                  | Express          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY8      | |SQUAR| CapSense | |SQUAR| Circuit  | |SQUAR| Co       | |SQUAR| Confi    | |SQUAR| D        |
| CMBR2044         | Express          | Housing          | nductive         | guration         | iplexing         |
|                  |                  |                  | Objects          |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Dynamic  | |SQUAR| EEPROM   | |SQUAR| EMI      | |SQUAR| ESD      | |SQUAR| Errors   | |SQUAR| FR4      |
| Reconfi          |                  |                  |                  |                  |                  |
| guration         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Filters  | |SQUAR| Finger   | |SQUAR| Flex PCB | |SQUAR| I2C      | |SQUAR| I2C-USB  | |SQUAR| IDAC     |
|                  | T                |                  |                  | Bridge           |                  |
|                  | hreshold         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| IMO and  | |SQUAR| ITO      | |SQUAR| Layout   | |SQUAR| Metal    | |SQUAR| Noise    | |SQUAR| Overlay  |
| P                |                  | Gu               |                  |                  |                  |
| rescaler         |                  | idelines         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PSoC3    | |SQUAR| P        | |SQUAR| Pa       | |SQUAR| Power    | |SQUAR| P        | |SQUAR| SNR      |
| CapSense         | arasitic         | thfinder         | Con              | roximity         |                  |
|                  | Cap              |                  | sumption         |                  |                  |
|                  | acitance         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SPI      | |SQUAR| Scanning | |SQUAR| S        | |SQUAR| Sensors  | |SQUAR| Shield   | |SQUAR| Sliders  |
|                  | Te               | chematic         |                  |                  |                  |
|                  | chniques         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Sm       | |SQUAR| Tuning   | |SQUAR| UART     | |SQUAR| Water    | |SQUAR| Water    |                  |
| artSense         |                  |                  |                  | Proofing         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**USB Controllers Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 8051     | |SQUAR| AN2131   | |SQUAR| AT2LP    | |SQUAR| ATA /    | |SQUAR| ATA      | |SQUAR| Asyn     |
|                  |                  |                  | ATAPI            | Commands         | chronous         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Auto     | |SQUAR| B        | |SQUAR| Blaster  | |SQUAR| Bulk     | |SQUAR| Bus      | |SQUAR| C#       |
| Mode             | andwidth         |                  | Transfer         | Power            |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| C++      | |SQUAR| CAT5     | |SQUAR| CF Card  | |SQUAR| CY3216   | |SQUAR| CY3649   | |SQUAR| CY3654   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3655   | |SQUAR| CY3660   | |SQUAR| CY3662   | |SQUAR| CY3664   | |SQUAR| CY3674   | |SQUAR| CY3681   |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY3684   | |SQUAR| CY3685   | |SQUAR| CY3686   | |SQUAR| CY4605   | |SQUAR| CY4606   | |SQUAR| CY4611B  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY4615   | |SQUAR| CYUSB    | |SQUAR| Clock    | |SQUAR| Co       | |SQUAR| Control  | |SQUAR| Control  |
|                  |                  |                  | mpliance         | Center           | Transfer         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Crystal  | |SQUAR| C        | |SQUAR| DLL      | |SQUAR| Debug    | |SQUAR| Des      | |SQUAR| Driver   |
|                  | yConsole         |                  |                  | criptors         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EEPROM   | |SQUAR| EZ-HOST  | |SQUAR| EZ-OTG   | |SQUAR| EZ-USB   | |SQUAR| E        | |SQUAR| EnCoreII |
|                  |                  |                  |                  | mulation         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| E        | |SQUAR| EnCoreV  | |SQUAR| Encore   | |SQUAR| Endpoint | |SQUAR| Enu      | |SQUAR| Errata   |
| nCoreIII         |                  |                  |                  | meration         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FIFO     | |SQUAR| FX       | |SQUAR| FX1      | |SQUAR| FX2      | |SQUAR| FX2LP    | |SQUAR| Firmware |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Firmware | |SQUAR| Flags    | |SQUAR| F        | |SQUAR| Full     | |SQUAR| GPIF     | |SQUAR| HDD      |
| Debug            |                  | ramework         | Speed            |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HID      | |SQUAR| HX2      | |SQUAR| HX2LP    | |SQUAR| Hi-Lo    | |SQUAR| High     | |SQUAR| Host     |
|                  |                  |                  | Pr               | Speed            | App              |
|                  |                  |                  | ogrammer         |                  | lication         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Hub      | |SQUAR| I2C      | |SQUAR| ICE      | |SQUAR| IN       | |SQUAR| I        | |SQUAR| In       |
|                  |                  |                  | Transfer         | nterrupt         | terrupts         |
|                  |                  |                  |                  | Transfer         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Iso      | |SQUAR| Keil     | |SQUAR| Keyboard | |SQUAR| Layout   | |SQUAR| Library  | |SQUAR| Loader   |
| chronous         |                  |                  |                  |                  |                  |
| Transfer         |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Low      | |SQUAR| M8A      | |SQUAR| M8B      | |SQUAR| Manual   | |SQUAR| Mass     | |SQUAR| Memory   |
| Speed            |                  |                  | Mode             | Storage          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Mouse    | |SQUAR| NX2LP    | |SQUAR| NX       | |SQUAR| Nand     | |SQUAR| Nand     | |SQUAR| OTP      |
|                  |                  | 2LP-Flex         | Flash            | Manuf            |                  |
|                  |                  |                  |                  | acturing         |                  |
|                  |                  |                  |                  | Utility          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| OUT      | |SQUAR| Port IO  | |SQUAR| Register | |SQUAR| Renu     | |SQUAR| Report   | |SQUAR| Reset    |
| Transfer         |                  |                  | meration         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SFR      | |SQUAR| SIE      | |SQUAR| SL811HS  | |SQUAR| SPI      | |SQUAR| SX2      | |SQUAR| S        |
|                  |                  |                  |                  |                  | chematic         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S        | |SQUAR| Screamer | |SQUAR| Script   | |SQUAR| Self     | |SQUAR| Slave    | |SQUAR| S        |
| chematic         |                  |                  | Power            | FIFO             | treaming         |
| Review           |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SuiteUSB | |SQUAR| Syn      | |SQUAR| TX2      | |SQUAR| TX2UL    | |SQUAR| Tetra    | |SQUAR| Th       |
|                  | chronous         |                  |                  | Hub              | roughput         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Timer    | |SQUAR| UART     | |SQUAR| UDMA     | |SQUAR| USBFS    | |SQUAR| USBUART  | |SQUAR| U        |
|                  |                  |                  |                  |                  | SBSerial         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Vendor   | |SQUAR| Video    | |SQUAR| WHQL     | |SQUAR| WLK      | |SQUAR| cyapi    | |SQUAR| uVision  |
| Command          | Class            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| **Super          |                  |                  |                  |                  |                  |
| Speed**          |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3      | |SQUAR| ADMux    | |SQUAR| ARM926EJ | |SQUAR| Bo       | |SQUAR| DMA      | |SQUAR| Eclipse  |
|                  |                  | -S               | otloader         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3 GPIO | |SQUAR| FX3      | |SQUAR| FX3      | |SQUAR| FX3 SDK  | |SQUAR| GPIF II  | |SQUAR| HS-OTG   |
|                  | Power            | Power            |                  |                  |                  |
|                  | Man              | supply           |                  |                  |                  |
|                  | agement          |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| JTAG     | |SQUAR| LPP      | |SQUAR| MSC      | |SQUAR| Os       | |SQUAR| RTOS     | |SQUAR| SD Card  |
|                  |                  |                  | cillator         |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S        | |SQUAR| USB 3.0  | |SQUAR| USB      | |SQUAR| USB Host | |SQUAR| UVC      |                  |
| lavefifo         |                  | Co               |                  |                  |                  |
|                  |                  | mpliance         |                  |                  |                  |
|                  |                  | Test             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FX3S     | |SQUAR| eMMC     | |SQUAR| SDIO     | |SQUAR| S-Ports  | |SQUAR| Bay      | |SQUAR| Benicia  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| HX3      | |SQUAR| ASSP     | |SQUAR| Battery  | |SQUAR| Bo       | |SQUAR| C        | |SQUAR| EEPROM   |
|                  |                  | charging         | otloader         | ortex-M0         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| GPIOs    | |SQUAR| H        | |SQUAR| H        | |SQUAR| I2C      | |SQUAR| I        | |SQUAR| Os       |
|                  | X3 Power         | X3 Power         |                  | n-system         | cillator         |
|                  | Man              | supply           |                  | pro              |                  |
|                  | agement          |                  |                  | gramming         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| USB 3.0  | |SQUAR| USB      |                  |                  |                  |                  |
| hub              | Shared           |                  |                  |                  |                  |
|                  | Link™            |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive MCU Tags (16FX/FR/FCR4/Traveo)**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Traveo   | |SQUAR| S6J3110  | |SQUAR| S6J3120  | |SQUAR| S6J3200  | |SQUAR| S6J3300  | |SQUAR| S6J3310  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J3320  | |SQUAR| S6J3330  | |SQUAR| S6J3340  | |SQUAR| S6J3350  | |SQUAR| S6J3400  | |SQUAR| S6J3510  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J326C  | |SQUAR| S6J324C  | |SQUAR| S6J327C  | |SQUAR| S6J328C  | |SQUAR| S6J32DA  | |SQUAR| S6J32BA  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6J3360  | |SQUAR| S6J3370  | |SQUAR| S6J32G0  | |SQUAR| MB9D560  | |SQUAR| CY9D560  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FR81S    | |SQUAR| MB91520  | |SQUAR| MB91550  | |SQUAR| MB91580  | |SQUAR| MB91570  | |SQUAR| MB91590  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY91520  | |SQUAR| CY91550  | |SQUAR| CY91580  | |SQUAR| CY91570  | |SQUAR| CY91590  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| FCR4     | |SQUAR| MB9DF125 | |SQUAR| MB9DF126 | |SQUAR| MB9EF226 | |SQUAR| CY9DF125 | |SQUAR| CY9DF126 |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CY9EF226 |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| 16FX     | |SQUAR| MB96600  | |SQUAR| CY96600  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Power    | |SQUAR| Current  | |SQUAR| PSS      | |SQUAR| Low      | |SQUAR| Mode     | |SQUAR| Clock    |
|                  |                  |                  | Power            |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Reset    | |SQUAR| External | |SQUAR| DDR      | |SQUAR| Hyper    |                  |                  |
|                  | Bus              | HSSPI            | Bus              |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CPU      | |SQUAR| MPU      | |SQUAR| PPU      | |SQUAR| TPU      |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Flash    | |SQUAR| RAM      | |SQUAR| Backup   | |SQUAR| DMA      | |SQUAR| I        |                  |
|                  |                  | RAM              |                  | nterrupt         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Port     | |SQUAR| A/D      | |SQUAR| D/A      | |SQUAR| Timer    | |SQUAR| Base     | |SQUAR| UDC/QPRC |
|                  | C                | C                |                  | Timer            |                  |
|                  | onverter         | onverter         |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Real     | |SQUAR| Watchdog |                  |                  |                  |                  |
| Time             |                  |                  |                  |                  |                  |
| clock            |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SHE      | |SQUAR| Security | |SQUAR| BootROM  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Multi-   | |SQUAR| UART     | |SQUAR| CSIO     | |SQUAR| I2C      | |SQUAR| CAN      | |SQUAR| CAN FD   |
| Function         |                  |                  |                  |                  |                  |
| Serial           |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| LIN      | |SQUAR| FlexRay  | |SQUAR| Ethernet |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CRC      |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Graphics | |SQUAR| Sound    | |SQUAR| SMC      | |SQUAR| LCDC     | |SQUAR| Media LB | APIX             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Audio    | |SQUAR| I2S      | |SQUAR| PCMPWM   |                  |                  |                  |
| DAC              |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Motor    | |SQUAR| Waveform | |SQUAR| RDC      | |SQUAR| MVA      |                  |                  |
| Control          | G                |                  |                  |                  |                  |
|                  | enerator         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| AUTOSAR  | |SQUAR| Safety   |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ev       | |SQUAR| Flash    | |SQUAR| Tool     | |SQUAR| Document | |SQUAR| Board    |                  |
| aluation         | Pr               |                  |                  |                  |                  |
| Kit              | ogrammer         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive MCU Tags (Traveo II)**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Traveo   |                  |                  |                  |                  |                  |
| II               |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Body     | |SQUAR| Gateway  | |SQUAR| Chassis  | |SQUAR| Cluster  |                  |                  |
| Control          |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CYT2B7   |                  |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Power    | |SQUAR| Power    | |SQUAR| Chip     | |SQUAR| Clock    | |SQUAR| Reset    |                  |
|                  | mode             | o                |                  |                  |                  |
|                  |                  | peration         |                  |                  |                  |
|                  |                  | mode             |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CPU      | |SQUAR| MPU      | |SQUAR| SMPU     | |SQUAR| PPU      | |SQUAR| SWPU     | |SQUAR| IPC      |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Flash    | |SQUAR| SRAM     | |SQUAR| ROM      | |SQUAR| DMA      | |SQUAR| I        |                  |
|                  |                  |                  |                  | nterrupt         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| IO       | |SQUAR| ADC      | |SQUAR| TCPWM    | |SQUAR| WDT      | |SQUAR| RTC      | |SQUAR| Trigger  |
| S                |                  |                  |                  |                  | Mul              |
| ubsystem         |                  |                  |                  |                  | tiplexer         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| CRYPTO   | |SQUAR| EVTGEN   | |SQUAR| EFUSE    | |SQUAR| Boot     |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| SCB      | |SQUAR| LIN      | |SQUAR| CAN/CAN  | |SQUAR| CXPI     |                  |                  |
|                  |                  | FD               |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Fault    | |SQUAR| Program  |                  |                  |                  |                  |
|                  | and              |                  |                  |                  |                  |
|                  | Debug IF         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| AUTOSAR  | |SQUAR| Safety   |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Ev       | |SQUAR| Flash    | |SQUAR| Tool     | |SQUAR| Document | |SQUAR| Board    |                  |
| aluation         | Pr               |                  |                  |                  |                  |
| Kit              | ogrammer         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive Power Management ICs (PMICs)Tags**

+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| PMIC     | |SQUAR| DC/DC    | |SQUAR| LDO      | |SQUAR| Buck     | |SQUAR| Boost    | |SQUAR| Bu       |
|                  | c                |                  |                  |                  | ck-boost         |
|                  | onverter         |                  |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| S6BP201A | |SQUAR| S6BP202A | |SQUAR| S6BP203A | |SQUAR| S6BP401A | |SQUAR| S6BP501A | |SQUAR| S6BP502A |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Channel  | |SQUAR| Input    | |SQUAR| Output   | |SQUAR| Load     | |SQUAR| Q        | |SQUAR| Ef       |
|                  | voltage          | voltage          | current          | uiescent         | ficiency         |
|                  |                  |                  |                  | current          |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| A        | |SQUAR| Fixed    | |SQUAR| Syn      | |SQUAR| Current  | |SQUAR| S        | |SQUAR| So       |
| utomatic         | PWM              | chronous         | mode             | witching         | ft-start         |
| PWM/PFM          |                  |                  |                  | f                |                  |
|                  |                  |                  |                  | requency         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Cold     | |SQUAR| MOSFET   | |SQUAR| Sc       | |SQUAR| PCB      | |SQUAR| Current  | |SQUAR| Line     |
| Crank            |                  | hematics         | Layout           | Sense            | Re               |
|                  |                  |                  |                  |                  | gulation         |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Load     | |SQUAR| Watchdog | |SQUAR| Power    | |SQUAR| UVLO     | |SQUAR| Thermal  | |SQUAR| SSCG     |
| re               | timer            | good             |                  | re               |                  |
| gulation         |                  |                  |                  | sistance         |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| OCP      | |SQUAR| OVP      | |SQUAR| Thermal  | |SQUAR| Ripple   | |SQUAR| Gain     | |SQUAR| Phase    |
|                  |                  | shutdown         |                  | margin           | margin           |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| Osc      | |SQUAR| Junction | |SQUAR| Ambient  | |SQUAR| BOM      | |SQUAR| Phase    | |SQUAR| Tem      |
| illation         | tem              | tem              |                  | comp             | perature         |
|                  | perature         | perature         |                  | ensation         | rise             |
+------------------+------------------+------------------+------------------+------------------+------------------+
| |SQUAR| EMI      | |SQUAR| R        | |SQUAR| Co       | |SQUAR| Snubber  |                  |                  |
|                  | adiation         | nduction         |                  |                  |                  |
|                  | noise            | noise            |                  |                  |                  |
+------------------+------------------+------------------+------------------+------------------+------------------+

**Automotive CXPI Tags**

+----------------------------+--------------------+------------+------------+------------+
| |SQUAR| CXPI Transceiver   |                    |            |            |            |
+----------------------------+--------------------+------------+------------+------------+
| |SQUAR| S6BT112A01         | |SQUAR| S6BT112A02 |            |            |            |
+----------------------------+--------------------+------------+------------+------------+

**Knowledge Base Article Type:** [Select the category by clicking the
checkbox associated]

+------------------------------------+------------------------------------------+
| |SQUAR| **KB Type-1**              | |SQUAR| **KB Type-2**                    |
+------------------------------------+------------------------------------------+
| |SQUAR| **Compiler**               | |SQUAR| **ByteCraft**                    |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Imagecraft**                   |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Keil**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **HiTech**                       |
+------------------------------------+------------------------------------------+
| |SQUAR| **Component Development**  | |SQUAR| **Training/Things you should     |
|                                    | know**                                   |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component Architecture**       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component/Project management** |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Datapath**                     |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Analog components**            |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Digital components**           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component software/tools**     |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Component Testing**            |
+------------------------------------+------------------------------------------+
| |SQUAR| **Development Kits**       | |SQUAR| **FirstTouch Kit**               |
+------------------------------------+------------------------------------------+
| |CRSQR| **Development Tools**      |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Device Drivers**         | |SQUAR| **Pullability**                  |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Mass Storage**                 |
+------------------------------------+------------------------------------------+
| |SQUAR| **Device Programming**     | |SQUAR| **PSoC Programmer**              |
+------------------------------------+------------------------------------------+
| |SQUAR| **Documentation**          |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Firmware**               |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **General**                |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Hardware**               | |SQUAR| **Digital**                      |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Specifications**               |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Analog**                       |
+------------------------------------+------------------------------------------+
| |SQUAR| **Known Problems and       |                                          |
| Solutions**                        |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Microcontrollers**       | |SQUAR| **8051**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **M8**                           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **M8C**                          |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Traveo**                       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Traveo II**                    |
|                                    +------------------------------------------+
|                                    | |SQUAR| **FCR4**                         |
|                                    +------------------------------------------+
|                                    | |SQUAR| **FR**                           |
|                                    +------------------------------------------+
|                                    | |SQUAR| **16FX**                         |
+------------------------------------+------------------------------------------+
| |SQUAR| **Modules**                | |SQUAR| **Automotive Power Management    |
|                                    | ICs (PMICs)**                            |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Automotive CXPI**              |
+------------------------------------+------------------------------------------+
| |SQUAR| **Platforms**              | |SQUAR| **MacOS X**                      |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Windows**                      |
+------------------------------------+------------------------------------------+
| |SQUAR| **Protocols**              | |SQUAR| **HID**                          |
|                                    +------------------------------------------+
|                                    | |SQUAR| **I2C**                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Quality**                |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Reference Designs**      |                                          |
+------------------------------------+------------------------------------------+
| |SQUAR| **Software**               | |SQUAR| **PSoC Designer**                |
|                                    +------------------------------------------+
|                                    | |SQUAR| **PSoC Creator**                 |
+------------------------------------+------------------------------------------+
| |SQUAR| **User Modules**           | |SQUAR| **Analog**                       |
|                                    +------------------------------------------+
|                                    | |SQUAR| **Digital**                      |
+------------------------------------+------------------------------------------+

DOCUMENT HISTORY
===================

Document Title: Managing the Makefile for ModusToolbox® v2.x – KBA229177

Document Number: 002-29177

+-------------+----------------+-------------------------------------+
|    **Rev.** |    **ECN No.** |    **Description of Change**        |
+=============+================+=====================================+
|    \*\*     |    6760595     |    Initial release                  |
+-------------+----------------+-------------------------------------+
|    \*A      |    6823491     |    Updated question 0               |
|             |                |    with the location of new BSP.    |
+-------------+----------------+-------------------------------------+
|    \*B      |    6885483     |    Updated question 0               |
|             |                |    to point to MTB User Guide (the  |
|             |                |    help information                 |
|             |                |    is relocated)                    |
+-------------+----------------+-------------------------------------+
|    \*C      |    6895151     |    Updated information regarding    |
|             |                |    the Eclipse IDE for ModusToolbox |
|             |                |    and the ModusToolbox User Guide. |
|             |                |                                     |
|             |                |    Updated Question 3 to include    |
|             |                |    the CY_EXTAPP_PATH variable.     |
|             |                |                                     |
|             |                |    Updated Question 6 to include    |
|             |                |    information about using forward  |
|             |                |    slashes in Windows System        |
|             |                |    Variables.                       |
|             |                |                                     |
|             |                |    Updated diagram in Question 13.  |
|             |                |                                     |
|             |                |    Combined Questions 17 and 18 to  |
|             |                |    refer to ModusToolbox User       |
|             |                |    Guide.                           |
|             |                |                                     |
|             |                |    Updated Questions 22 and 23 to   |
|             |                |    refer to ModusToolbox User       |
|             |                |    Guide.                           |
+-------------+----------------+-------------------------------------+



.. |image1| image:: ../_static/image/tool-guide/manging_makefile/image1.jpg
