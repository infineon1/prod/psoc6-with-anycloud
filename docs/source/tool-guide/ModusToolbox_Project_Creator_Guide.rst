===================================
ModusToolbox Project Creator Guide
===================================

Overview
==========

The Project Creator tool is used with the ModusToolbox software to
create projects based on Board Support Packages (BSPs) and code
examples. It is provided as a `graphical user interface
(GUI) <#gui-description>`__ and a `command-line interface (CLI) <#cli-description>`__ 
to create projects for use in any software environment
you prefer. The tool essentially runs a “git clone” command to create
projects onto your computer from a remote server. The GUI provides a
simple interface for users who do not want to use the CLI.

Launch the Project Creator
===========================

You can launch the Project Creator using the Eclipse IDE for
ModusToolbox. You can also launch it as a stand-alone GUI tool and from
the command line.

.. note::

   If you prefer to use the Eclipse IDE, you should launch Project
   Creator tool from the Eclipse IDE included with ModusToolbox instead of
   as a stand-alone tool or from the command line.

From Eclipse IDE
=================

The Eclipse IDE included with ModusToolbox provides links and menu items
to launch the Project Creator tool and create a project that applies to
Eclipse.

|image1|

For more information, refer to the Eclipse IDE for ModusToolbox `Quick
Start Guide <http://www.cypress.com/ModusToolboxQSG>`__ or `User
Guide. <http://www.cypress.com/ModusToolboxUserGuide>`__

As a Stand-Alone Tool
----------------------

To run the Project Creator as a stand-alone tool, navigate to the
install location and run the *project-creator* executable to launch the
GUI or run the *project-creator-cli* executable for the CLI. On Windows,
the default install location for the tool is:

   *[user_home]/ModusToolbox/tools_<version>/project-creator*

For other operating systems, the installation directory will vary, based
on how the software was installed.


GUI Description
================

The Project Creator GUI provides a series of pages used to create a
project. The GUI executes the CLI options based on your selections.

Menus
======

The Project Creator has two menus, as follows:

   Settings

-  Offline – Check box to switch to offline mode and read the local copy
   of the manifest file installed from the offline content bundle.
   Refer to the *ModusToolbox User Guide* for more details.

-  Proxy Settings – Opens a dialog to specify direct or manual proxy
   settings.

|image2|

   Help

-  View Help – Opens this document.

-  About – Displays tool version information.

Choose BSP Page
================

When you first launch the GUI, it displays the “Choose Board Support
Package (BSP)” page that lists the available BSPs.

|image3|

Import BSP
===========

The Choose BSP page provides an **Import** button |image9| to select any
custom BSPs you may have created or received from a colleague. You can
then create a new application for that custom BSP.

After clicking the **Import** button, in the Select Folder dialog,
select the appropriate custom BSP to use for your new application. Then,
the custom BSP will be shown with all the other BSPs.

|image4|

For information about creating custom BSPs, refer to the *ModusToolbox
User Guide*.

Select Application Page
========================

Select a board on the Choose BSP page and click **Next >** to display
the “Select Application” page.

|image5|

On this page:

   Select an application from the list

   Enter an **Application Name**, or leave it as the default.

   Specify a **Location**, or leave it as the default. All files and
   folders from the selected application will be copied to this location
   in a folder with the **Application Name**.



Import Application
===================

The Select Application page also provides an **Import** button |image10|
to select other applications you may have created or received from a
colleague. After clicking the button, in the Select Folder dialog,
select only examples that are supported by the BSP you selected for this
application. Then, the example will be shown in the with all the other
applications.

|image6|

Create an Application
======================

Click **Create** to start the process of creating a ModusToolbox
application, and the GUI will display various messages showing the
progress.

|image7|

When the process competes, the tool will display a message similar to
the following:

|image8|

You can continue creating new projects from the “Select Application”
page by selecting a new application and then clicking the **Create**
button again. You can also go back to the “Choose BSP” page to select
another BSP and then continue the project creation process as usual.

To close the tool, click the **Close** button or the **X** button at the
top-right of the GUI.



CLI Description
================

The CLI allows creating projects from a command-line prompt or from
within batch files or shell scripts. The exit code for the CLI is zero
if the operation is successful, or non-zero if the operation encounters
an error.

Usage
------

   project-creator-cli [options]

Options
--------

To see a list of options, run the executable with the -h option.

Example
--------

   ./project-creator-cli \\

   --board-id CY8CKIT-062-WIFI-BT \\

   --app-id BlinkyLED \\

   --user-app-name MyLED \\

   --target-dir "C:/cypress_projects"

Created Project
================

As mentioned previously, the Project Creator tool essentially runs a
“git clone” command to create projects onto your computer from a remote
server. Different types of projects will have different files and folder
structures. After creating the project, navigate to the newly specified
folder that was created on disk. Inspect the files and folders located
there. Use these files to modify your application as necessary in your
preferred environment.

Version Changes
================

This section lists and describes the changes for each version of this
tool.

+------------+------------------------------------------------+
|**Version** |   **Change Descriptions**                      |
+============+================================================+
|1.0         | New tool.                                      |
+------------+------------------------------------------------+
|1.1         | Redesigned the GUI to provide a better flow.   |
|            +------------------------------------------------+
|            | Added Settings and Help menus.                 |
|            +------------------------------------------------+
|            | Added icon to indicate online/offline status.  |
|            +------------------------------------------------+
|            | Added Import BSP button.                       |
+------------+------------------------------------------------+





.. |image1| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator1.png
.. |image2| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator2.png
.. |image3| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator3.png
.. |image4| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator4.png
.. |image5| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator5.png
.. |image6| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator6.png
.. |image7| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator7.png
.. |image8| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator8.png
.. |image9| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator9.png
.. |image10| image:: ../_static/image/Modustoolbox/ModusToolbox-Project-Creator-Guide/project_creator9.png
